package edu.ualr.jrming.droidgrr;

/**
 * LogcatWatcher.java
 * @author Jonathan Ming
 * Modified version of LogcatWatcher.java, authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** This class detects third-party app logs (filtered logcat output). **/
public abstract class LogcatWatcher { //TODO: Doesn't really work thanks to 4.1 Logcat prohibition
	// Initialize constants and variables
	private static final String TAG = "LogcatWatcher";

	public static void retrieveLogs(Context context)  {
		try {
			// Initialize variables
			String line;
			Pattern pidPattern = Pattern.compile("\\([ 0-9]{5}\\):");
			Pattern datePattern = Pattern.compile("\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\.\\d{3}");
			Matcher pidMatcher;
			Matcher dateMatcher;
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			Uri eventsUri = DroidGRRProvider.Events.CONTENT_URI;
			
			// Get running processes
			ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
			// TODO: Use SparseArray for better performance
			HashMap<Integer,String> processMap = new HashMap<>();
			for(RunningAppProcessInfo proc:am.getRunningAppProcesses()) {
				processMap.put(proc.pid, proc.processName);
			}
			
			// Filter out some of the built-in app activity (noise)
			String filters = " BatteryService:S StatusBarPolicy:S PhoneUtils:S " +
					"NotificationService:S AlarmManagerService:S dalvikvm:S ActivityManager:S " +
					"Email:S LogsProvider:S Zygote:S Database:S ConnectivityService:S " +
					"WifiApBroadcastReceiver:S PlayerDriver:S PVPlayer:S DMApp:S ServiceManager:S " +
					"SyncmlBootReceiver:S SAN_SERVICE:S SyncmlService:S SANService:S AlarmManager:S " +
					"EventLogService:S AudioTrack:S SensorManager:S NetworkStateTracker:S " +
					"GTalkService:S lights:S MediaPlayer:S ActivityThread:S pppd:S dhcpcd:S [FT]-Server:S " +
					"Tethering:S TelephonyRegistry:S System.out:S RingtoneManager:S libnetutils:S " +
					"TextToSpeech:S SynthProxy:S ProcessStats:S BrowserSettings:S dalvikvm-heap:S " +
					"AudioService:S PhotoAppWidgetProvider:S Launcher:S Settings:S " +
					"PowerManagerService:S OrientationDebug:S OmaDrmConfigService:S " +
					"ScreenCaptureAction:S WindowManager:S InputManagerService:S GLThread:S " +
					"ALSAModule:S InputReader:S GlassLockScreen:S KeyguardViewMediator:S " +
					"AudioPolicyManager:S AudioFlinger:S PackageManager:S DmAppInfo:S DebugDb:S " +
					"PackageInfoItemFactory:S DebugPlacement:S EglHelper:S Main:S KeyCharacterMap:S " +
					"MediaExtractor:S OggExtractor:S OMXCodec:S PhoneInfoReceiver:S PackageInfoHelper:S " +
					"FeatureCheckerImpl:S IconDebug:S DebugFolder:S installd:S ThermalEngine:S " +
					"MotoNetwCtrlr:S NetlinkEvent:S NetdConnector:S BrcmNfcNfa:S OpenGLRenderer:S *:I";

			// Execute logcat
			Process process = Runtime.getRuntime().exec("logcat -d -v time *:V");// + filters);
			
			// Parse logcat output line by line
			String found;
			int pid;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			Log.v(TAG,"Attempting to read and store Logcat.");

			while ((line = bufferedReader.readLine()) != null) {
				// Get the process information that generated the line
				pidMatcher = pidPattern.matcher(line);
				if (pidMatcher.find()) {
					found = pidMatcher.group(0);
					found = found.replaceAll("[() :]", "");
					try {
						pid = Integer.parseInt(found);
					}
					catch(Exception e) {
						continue;
					}
					
					// Get processName running at a given PID
					String appName = processMap.get(pid);
					
					// Skip processes with no name
					if (appName == null) {
						continue;
					}
					
					// Skip known built-in processes
					if (appName.equals("system") || appName.equals("edu.ualr.jrming.droidgrr")
							|| appName.equals("com.android.MtpApplication") || appName.equals("com.sec.android.app.twlauncher")
							|| appName.equals("com.android.packageinstaller") || appName.equals("android.tts")
							|| appName.startsWith("com.android") || appName.startsWith("com.google")
							|| appName.startsWith("com.sec.android")) {
						continue;
					}
					
					// Get date from the log event
					dateMatcher = datePattern.matcher(line);
					if (dateMatcher.find()) {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
						String logDate = dateMatcher.group(0);
						logDate=year+"-"+logDate;
						Date date = df.parse(logDate);
						
						// Insert third-party app log into DroidWatch
						ContentValues values = new ContentValues();
						values.put(DroidGRRDatabase.DETECTOR_COLUMN, TAG);
						values.put(DroidGRRDatabase.EVENT_ACTION_COLUMN, "Logcat");
						values.put(DroidGRRDatabase.EVENT_DATE_COLUMN, date.getTime());
						values.put(DroidGRRDatabase.EVENT_DESCRIPTION_COLUMN, appName);
						values.put(DroidGRRDatabase.ADDITIONAL_INFO_COLUMN, line);
						context.getContentResolver().insert(eventsUri, values);
					}
				}
			}
			
			// Clear logcat logs
			bufferedReader.close();
			Runtime.getRuntime().exec("logcat -c");
		}
		catch(Exception e) {
			Log.e(TAG, "Unable to run logcat: "+e.getMessage());
			//return;
		}
    }
}
