package edu.ualr.jrming.droidgrr;

/**
 * WatcherButtonFragment.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/** A PreferenceFragment containing the Watcher launch buttons */
public class WatcherButtonFragment extends PreferenceFragment
        implements Preference.OnPreferenceClickListener {

    // ArrayList to hold references to each of the preferences (for click listener registration)
    private ArrayList<Preference> mWatcherPrefs = new ArrayList<>(15);


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_watchers);
        setHasOptionsMenu(false);

        // Populate preference ArrayList
        mWatcherPrefs.add(findPreference("watcher_device_account"));
        mWatcherPrefs.add(findPreference("watcher_app"));
        mWatcherPrefs.add(findPreference("watcher_calendar"));
        mWatcherPrefs.add(findPreference("watcher_callLog"));
        mWatcherPrefs.add(findPreference("watcher_sms"));
        mWatcherPrefs.add(findPreference("watcher_mms"));
        mWatcherPrefs.add(findPreference("watcher_contact"));
        mWatcherPrefs.add(findPreference("watcher_gallery"));
        mWatcherPrefs.add(findPreference("watcher_location"));
        mWatcherPrefs.add(findPreference("watcher_screen"));
        mWatcherPrefs.add(findPreference("watcher_logcat"));

        // Register preference click listeners
        for (Preference watcher : mWatcherPrefs) {
            if(watcher != null) {
                watcher.setOnPreferenceClickListener(this);
            }
        }
    }


    @Override
    public boolean onPreferenceClick(Preference preference) {
        switch(preference.getKey()) {
            case "watcher_device_account":
                WatcherIntentService.queryDeviceAccounts(getActivity());
                break;

            case "watcher_app":
                Toast.makeText(getActivity(), "App Install/Removal recording is automatic", Toast.LENGTH_SHORT).show();
                break;

            case "watcher_calendar":
                WatcherIntentService.queryCalendar(
                        getActivity(),
                        System.currentTimeMillis()-172800000,
                        System.currentTimeMillis()
                );
                break;

            case "watcher_callLog":
                WatcherIntentService.queryCallLog(
                        getActivity(),
                        System.currentTimeMillis()-172800000,
                        System.currentTimeMillis()
                );
                break;

            case "watcher_sms":
                WatcherIntentService.querySmsLog(
                        getActivity(),
                        System.currentTimeMillis()-172800000,
                        System.currentTimeMillis()
                );
                break;

            case "watcher_mms":
                WatcherIntentService.queryMmsLog(
                        getActivity(),
                        System.currentTimeMillis()-172800000,
                        System.currentTimeMillis()
                );
                break;

            case "watcher_contact":
                WatcherIntentService.queryContacts(getActivity());
                break;

            case "watcher_gallery":
                WatcherIntentService.queryPhotos(
                        getActivity(),
                        System.currentTimeMillis()-172800000,
                        System.currentTimeMillis()
                );
                break;

            case "watcher_location":
                WatcherIntentService.queryLocation(getActivity());
                break;

            case "watcher_screen":
                Toast.makeText(getActivity(), "App Install/Removal recording is automatic", Toast.LENGTH_SHORT).show();
                break;

            case "watcher_logcat":
                int logPerm = ActivityCompat.checkSelfPermission(
                        getActivity(), Manifest.permission.READ_LOGS
                );
                if (logPerm == PackageManager.PERMISSION_GRANTED) {
                    //WatcherIntentService.queryLogcat(getActivity());
                    Toast.makeText(getActivity(), "Logcat watcher not functional", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Denied permission to access logs.", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                return false;
        }

        return true;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            ListView listView = (ListView) getView().findViewById(android.R.id.list);
            Adapter adapter = listView.getAdapter();

            if (adapter != null) {
                int height = 0;
                //int height = listView.getPaddingTop() + listView.getPaddingBottom();
                for (int i = 0; i < adapter.getCount(); i++) {
                    View item = adapter.getView(i, null, listView);
                    item.measure(0, 0);
                    height += item.getMeasuredHeight();
                }

                FrameLayout frame = (FrameLayout) getActivity().findViewById(R.id.frame2);
                ViewGroup.LayoutParams param = frame.getLayoutParams();
                param.height = height + (listView.getDividerHeight() * adapter.getCount());
                frame.setLayoutParams(param);
            }
        }
    }
}