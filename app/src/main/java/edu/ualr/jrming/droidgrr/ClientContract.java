package edu.ualr.jrming.droidgrr;

/**
 * ClientContract.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A contract holding constants used for accessing client-specific data stored outside the app,
 * either in SharedPreferences or AndroidKeyStore.
 */
public interface ClientContract {

    // SharedPreference Constants:
    /** Name of the client preferences file. */
    String CLIENT_PREFS = "GrrClientPrefs";
        /** Key for obtaining the client's X.509 common name */
        String CLIENT_CN = "clientCommonName";
        /** Key for obtaining the next ID base to use in task ID generation */
        String NEXT_ID_BASE = "nextMessageIdBase";

    // KeyStore Constants:
    /** Alias under which the client's RSA KeyPair is stored in the Android Keystore */
    String CLIENT_KEYS = "clientKeys";
    /** Alias under which the server's certificate is stored once verified */
    String SERVER_CERT = "serverCertificate";
}
