package edu.ualr.jrming.droidgrr;

/**
 * DroidGRRProvider.java
 * @author Jonathan Ming
 * Modified version of DroidWatchProvider.java, authored by Justin Grover
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

/** This class handles content provider functions required of the DroidWatch database. **/
public class DroidGRRProvider extends ContentProvider {
	private static final String TAG = "DroidGRRProvider";
	private DroidGRRDatabase db;
	private static final String AUTHORITY = "edu.ualr.jrming.droidgrr.DroidGRRProvider";
	public static final int EVENTS = 0;
	public static final int EVENT_ID = 1;
	public static final int TRANSFERS = 2;
	public static final int TRANSFER_ID = 3;
	public static final int CONTACTS = 4;
	public static final int CONTACT_ID = 5;
	public static final int CALENDAR = 6;
	public static final int CALENDAR_ID = 7;
	public static final int SMS = 8;
	public static final int SMS_ID = 9;
	public static final int PHOTOS = 10;
	public static final int PHOTO_ID = 11;
	public static final int MMS = 12;
	public static final int MMS_ID = 13;

	private static final UriMatcher URI_MATCHER;
	
	/** This interface lists available columns for the events table. **/
	public interface Events extends BaseColumns {
		String CONTENT_PATH = "events";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.event";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.event";
		String DETECTOR = DroidGRRDatabase.DETECTOR_COLUMN;
	    String DETECTED = DroidGRRDatabase.DETECT_DATE_COLUMN;
	    String ACTION = DroidGRRDatabase.EVENT_ACTION_COLUMN;
	    String EVENT_DATE = DroidGRRDatabase.EVENT_DATE_COLUMN;
	    String DESCRIPTION = DroidGRRDatabase.EVENT_DESCRIPTION_COLUMN;
	    String ADDITIONAL_INFO = DroidGRRDatabase.ADDITIONAL_INFO_COLUMN;
		String SORT_ORDER_DEFAULT = _ID+" ASC";
	}
	
	/** This interface lists available columns for the transfers table. **/
	public interface Transfers extends BaseColumns {
		String CONTENT_PATH = "transfers";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.transfer";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.transfer";
		String COMPLETED = DroidGRRDatabase.TRANSFERS_COMPLETED_COLUMN;
		String STARTDATE = DroidGRRDatabase.TRANSFERS_STARTDATE_COLUMN;
		String DEVICE_ID = DroidGRRDatabase.TRANSFERS_DEVICE_ID_COLUMN;
	}
	
	/** This interface lists available columns for the calendar table. **/
	public interface Calendar extends BaseColumns {
		String CONTENT_PATH = "calendar";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.calendar";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.calendar";
		String ID = DroidGRRDatabase.CALENDAR_EVENT_ID_COLUMN;
		String NAME = DroidGRRDatabase.CALENDAR_EVENT_NAME_COLUMN;
		String EVENT_DATE = DroidGRRDatabase.CALENDAR_EVENT_DATE_COLUMN;
		String DATE_ADDED = DroidGRRDatabase.CALENDAR_EVENT_ADDED_COLUMN;
		String SORT_ORDER_DEFAULT = DATE_ADDED+" DESC";
	}
	
	/** This interface lists available columns for the contacts table. **/
	public interface Contacts extends BaseColumns {
		String CONTENT_PATH = "contacts";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.contact";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.contact";
		String ID = DroidGRRDatabase.CONTACT_ID_COLUMN;
		String NAME = DroidGRRDatabase.CONTACT_NAME_COLUMN;
		String NUMBER = DroidGRRDatabase.CONTACT_NUMBER_COLUMN;
		String ADDED = DroidGRRDatabase.CONTACT_ADDED_COLUMN;
		String SORT_ORDER_DEFAULT = ADDED+" DESC";
	}

	/** This interface lists available columns for the SMS table. **/
	public interface Sms extends BaseColumns {
		String CONTENT_PATH = "sms";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.sms";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.sms";
		String MSG_ID = DroidGRRDatabase.SMS_MSG_ID_COLUMN;
		String THREAD_ID = DroidGRRDatabase.SMS_THREAD_ID_COLUMN;
		String ADDRESS = DroidGRRDatabase.SMS_ADDRESS_COLUMN;
		String CONTACT = DroidGRRDatabase.SMS_CONTACT_COLUMN;
		String DATE = DroidGRRDatabase.SMS_DATE_COLUMN;
		String TYPE = DroidGRRDatabase.SMS_TYPE_COLUMN;
		String BODY = DroidGRRDatabase.SMS_BODY_COLUMN;
		String SORT_ORDER_DEFAULT = DATE + " DESC";
	}

	/** This interface lists available columns for the MMS table. **/
	public interface Mms extends BaseColumns {
		String CONTENT_PATH = "mms";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.mms";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.mms";
		String MSG_ID = DroidGRRDatabase.MMS_MSG_ID_COLUMN;
		String THREAD_ID = DroidGRRDatabase.MMS_THREAD_ID_COLUMN;
		String ADDRESSES = DroidGRRDatabase.MMS_ADDRESSES_COLUMN;
		String DATE = DroidGRRDatabase.MMS_DATE_COLUMN;
		String BOX = DroidGRRDatabase.MMS_BOX_COLUMN;
		String MSG_CONTENT_TYPE = DroidGRRDatabase.MMS_CONTENT_TYPE_COLUMN;
		String TEXT = DroidGRRDatabase.MMS_TEXT_COLUMN;
		String DATA = DroidGRRDatabase.MMS_DATA_COLUMN;
		String SORT_ORDER_DEFAULT = DATE + " DESC";
	}

	/** This interface lists available columns for the Photos table. **/
	public interface Photos extends BaseColumns {
		String CONTENT_PATH = "photos";
		Uri CONTENT_URI = Uri.parse("content://"+ DroidGRRProvider.AUTHORITY+"/"+CONTENT_PATH);
		String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.droidgrr.photo";
		String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.droidgrr.photo";
		String PHOTO_ID = DroidGRRDatabase.PHOTO_ID_COLUMN;
		String FILEPATH = DroidGRRDatabase.PHOTO_FILEPATH_COLUMN;
		String DATE = DroidGRRDatabase.PHOTO_DATE_COLUMN;
		String LATITUDE = DroidGRRDatabase.PHOTO_LATITUDE_COLUMN;
		String LONGITUDE = DroidGRRDatabase.PHOTO_LONGITUDE_COLUMN;
		String SORT_ORDER_DEFAULT = DATE + " DESC";
	}

	static {
		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		URI_MATCHER.addURI(AUTHORITY, Events.CONTENT_PATH, EVENTS);
		URI_MATCHER.addURI(AUTHORITY, Events.CONTENT_PATH + "/#", EVENT_ID);
		URI_MATCHER.addURI(AUTHORITY, Transfers.CONTENT_PATH, TRANSFERS);
		URI_MATCHER.addURI(AUTHORITY, Transfers.CONTENT_PATH+"/#", TRANSFER_ID);
		URI_MATCHER.addURI(AUTHORITY, Contacts.CONTENT_PATH, CONTACTS);
		URI_MATCHER.addURI(AUTHORITY, Contacts.CONTENT_PATH+"/#", CONTACT_ID);
		URI_MATCHER.addURI(AUTHORITY, Calendar.CONTENT_PATH, CALENDAR);
		URI_MATCHER.addURI(AUTHORITY, Calendar.CONTENT_PATH+"/#", CALENDAR_ID);
		URI_MATCHER.addURI(AUTHORITY, Sms.CONTENT_PATH, SMS);
		URI_MATCHER.addURI(AUTHORITY, Sms.CONTENT_PATH+"/#", SMS_ID);
		URI_MATCHER.addURI(AUTHORITY, Mms.CONTENT_PATH, MMS);
		URI_MATCHER.addURI(AUTHORITY, Mms.CONTENT_PATH+"/#", MMS_ID);
		URI_MATCHER.addURI(AUTHORITY, Photos.CONTENT_PATH, PHOTOS);
		URI_MATCHER.addURI(AUTHORITY, Photos.CONTENT_PATH+"/#", PHOTO_ID);
	}

	@Override
	public boolean onCreate() {
		db = new DroidGRRDatabase(getContext());
		return true;
	}

	/**
	 * This method returns the type of a content URI.
	 * @param uri	The content provider URI.
	 */
	@Override
	public String getType(@NonNull Uri uri) {
		// Determine the URI and associate with a type.
		switch (URI_MATCHER.match(uri)) {
			case EVENTS:
				return Events.CONTENT_TYPE;
			case EVENT_ID:
				return Events.CONTENT_ITEM_TYPE;
			case TRANSFERS:
				return Transfers.CONTENT_TYPE;
			case TRANSFER_ID:
				return Transfers.CONTENT_ITEM_TYPE;
			case CONTACTS:
				return Contacts.CONTENT_TYPE;
			case CONTACT_ID:
				return Contacts.CONTENT_ITEM_TYPE;
			case CALENDAR:
				return Calendar.CONTENT_TYPE;
			case CALENDAR_ID:
				return Calendar.CONTENT_ITEM_TYPE;
			case SMS:
				return Sms.CONTENT_TYPE;
			case SMS_ID:
				return Sms.CONTENT_ITEM_TYPE;
			case MMS:
				return Mms.CONTENT_TYPE;
			case MMS_ID:
				return Mms.CONTENT_ITEM_TYPE;
			case PHOTOS:
				return Photos.CONTENT_TYPE;
			case PHOTO_ID:
				return Photos.CONTENT_ITEM_TYPE;
		    default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}


	/**
	 * This method provides a Cursor to receive queried data.
	 * @param uri			The content provider URI.
	 * @param projection	The columns to be returned.
	 * @param selection		The content fields selected.
	 * @param selectionArgs	The content selected.
	 * @param sortOrder		The sorting order.
	 */
	@Override
	public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		// Determine the content provider URI and perform the associated selection
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		switch (URI_MATCHER.match(uri)) {
			case EVENT_ID:
				queryBuilder.setTables(DroidGRRDatabase.EVENTS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Events.SORT_ORDER_DEFAULT;
				}
				queryBuilder.appendWhere(Events._ID + "=" + uri.getLastPathSegment());
				break;
			case EVENTS:
				queryBuilder.setTables(DroidGRRDatabase.EVENTS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Events.SORT_ORDER_DEFAULT;
				}
				break;
			case TRANSFER_ID:
			case TRANSFERS:
				queryBuilder.setTables(DroidGRRDatabase.TRANSFERS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = null;
				}
				break;
			case CONTACT_ID:
			case CONTACTS:
				queryBuilder.setTables(DroidGRRDatabase.CONTACTS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Contacts.SORT_ORDER_DEFAULT;
				}
				break;
			case CALENDAR_ID:
			case CALENDAR:
				queryBuilder.setTables(DroidGRRDatabase.CALENDAR_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Calendar.SORT_ORDER_DEFAULT;
				}
				break;
			case SMS_ID:
			case SMS:
				queryBuilder.setTables(DroidGRRDatabase.SMS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Sms.SORT_ORDER_DEFAULT;
				}
				break;
			case MMS_ID:
			case MMS:
				queryBuilder.setTables(DroidGRRDatabase.MMS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Mms.SORT_ORDER_DEFAULT;
				}
				break;
			case PHOTO_ID:
			case PHOTOS:
				queryBuilder.setTables(DroidGRRDatabase.PHOTOS_TABLE);
				if (TextUtils.isEmpty(sortOrder)) {
					sortOrder = Photos.SORT_ORDER_DEFAULT;
				}
				break;
			default:
				throw new IllegalArgumentException("Unknown URI");
		}

		SQLiteDatabase sqlDB = db.getReadableDatabase();

		Cursor cursor = queryBuilder.query(
				sqlDB,			// The database to select
				projection,		// The columns to select
				selection,		// Any WHERE clauses (including ID) with ?s for values
				selectionArgs,	// Values to substitute for WHERE ?s
				null,null,		// No GROUP BY or HAVING clauses
				sortOrder		// ORDER BY clause
		);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}


	/**
	 * This method provides the insertion capabilities to available content provider URIs.
	 * @param uri			The content provider URI.
	 * @param values		Key:value pairs.
	 */
	@Override
	public Uri insert(@NonNull Uri uri, ContentValues values) {
		// Determine the content provider URI and perform the associated insertion.
		String tableName;
		switch (URI_MATCHER.match(uri)) {
		    case EVENTS:
		   		tableName = DroidGRRDatabase.EVENTS_TABLE;
		   		break;
		    case TRANSFERS:
		   		tableName = DroidGRRDatabase.TRANSFERS_TABLE;
		   		break;
		    case CONTACTS:
		   		tableName = DroidGRRDatabase.CONTACTS_TABLE;
		   		break;
		    case CALENDAR:
		   		tableName = DroidGRRDatabase.CALENDAR_TABLE;
		  		break;
			case SMS:
				tableName = DroidGRRDatabase.SMS_TABLE;
				break;
			case MMS:
				tableName = DroidGRRDatabase.MMS_TABLE;
				break;
			case PHOTOS:
				tableName = DroidGRRDatabase.PHOTOS_TABLE;
				break;
		    default:
		    	throw new IllegalArgumentException("Unsupported URI for insertion: " + uri);
		}
		Log.v(TAG,"Inserting into the database: "+tableName+" | "+values.toString());

		SQLiteDatabase sqlDB = db.getWritableDatabase();
		long id = sqlDB.insert(tableName, null, values);
		if (id > 0) {
			  Uri itemUri = ContentUris.withAppendedId(uri, id);
			  getContext().getContentResolver().notifyChange(itemUri, null);
			  return itemUri;
		}
		throw new SQLException("Problem while inserting into events table");
	}


	/**
	 * This method provides the update capabilities to available content provider URIs.
	 * @param uri			The content provider URI.
	 * @param values		Key:value pairs.
	 * @param selection		The content fields selected.
	 * @param selectionArgs	The content selected.
	 */
	@Override
	public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase sqlDB = db.getWritableDatabase();
		int updateCount;

		// Determine the content provider URI and perform the associated update
		switch (URI_MATCHER.match(uri)) {
			case EVENTS:
				updateCount = sqlDB.update(DroidGRRDatabase.EVENTS_TABLE, values, selection, selectionArgs);
				break;
			case EVENT_ID:
				String eventID = uri.getLastPathSegment();  
				String eventWhereClause = Events._ID + " = " + eventID;
				if (!TextUtils.isEmpty(selection)) {
					eventWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.EVENTS_TABLE, values, eventWhereClause, selectionArgs);
				break;
			case TRANSFERS:
				updateCount = sqlDB.update(DroidGRRDatabase.TRANSFERS_TABLE, values, selection, selectionArgs);
				break;
			case TRANSFER_ID:
				String transferID = uri.getLastPathSegment();
				String transferWhereClause = Transfers._ID + " = " + transferID;
				if (!TextUtils.isEmpty(selection)) {
					transferWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.TRANSFERS_TABLE, values, transferWhereClause, selectionArgs);
				break;
			case CONTACTS:
				updateCount = sqlDB.update(DroidGRRDatabase.CONTACTS_TABLE, values, selection, selectionArgs);
				break;
			case CONTACT_ID:
				String contactID = uri.getLastPathSegment();
				String contactWhereClause = Contacts.ID + " = " + contactID;
				if (!TextUtils.isEmpty(selection)) {
					contactWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.CONTACTS_TABLE, values, contactWhereClause, selectionArgs);
				break;
			case CALENDAR:
				updateCount = sqlDB.update(DroidGRRDatabase.CALENDAR_TABLE, values, selection, selectionArgs);
				break;
			case CALENDAR_ID:
				String calendarID = uri.getLastPathSegment();
				String calendarWhereClause = Calendar.ID + " = " + calendarID;
				if (!TextUtils.isEmpty(selection)) {
					calendarWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.CALENDAR_TABLE, values, calendarWhereClause, selectionArgs);
				break;
			case SMS:
				updateCount = sqlDB.update(DroidGRRDatabase.SMS_TABLE, values, selection, selectionArgs);
				break;
			case SMS_ID:
				String smsID = uri.getLastPathSegment();
				String smsWhereClause = Sms.MSG_ID + " = " + smsID;
				if(!TextUtils.isEmpty(selection)) {
					smsWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.SMS_TABLE, values, smsWhereClause, selectionArgs);
				break;
			case MMS:
				updateCount = sqlDB.update(DroidGRRDatabase.MMS_TABLE, values, selection, selectionArgs);
				break;
			case MMS_ID:
				String mmsID = uri.getLastPathSegment();
				String mmsWhereClause = Mms.MSG_ID + " = " + mmsID;
				if(!TextUtils.isEmpty(selection)) {
					mmsWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.MMS_TABLE, values, mmsWhereClause, selectionArgs);
				break;
			case PHOTOS:
				updateCount = sqlDB.update(DroidGRRDatabase.PHOTOS_TABLE, values, selection, selectionArgs);
				break;
			case PHOTO_ID:
				String photoID = uri.getLastPathSegment();
				String photoWhereClause = Photos.PHOTO_ID + " = " + photoID;
				if(!TextUtils.isEmpty(selection)) {
					photoWhereClause += " AND " + selection;
				}
				updateCount = sqlDB.update(DroidGRRDatabase.PHOTOS_TABLE, values, photoWhereClause, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		
		if (updateCount > 0) {
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return updateCount;
	}

	/**
	 * This method provides the deletion capabilities to available content provider URIs.
	 * @param uri			The content provider URI.
	 * @param selection		The content fields selected.
	 * @param selectionArgs	The content selected.
	 */
	@Override
	public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase sqlDB = db.getWritableDatabase();
		int deleteCount;

		// Associate an action with a content provider URI
		switch (URI_MATCHER.match(uri)) {
			case EVENTS:
				deleteCount = sqlDB.delete(DroidGRRDatabase.EVENTS_TABLE, selection, selectionArgs);
				break;
			case EVENT_ID:
				String eventID = uri.getLastPathSegment();
				String eventWhereClause = Events._ID + " = " + eventID;
				if (!(TextUtils.isEmpty(selection))) {
					eventWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.EVENTS_TABLE, eventWhereClause, selectionArgs);
				break;
			case TRANSFERS:
				deleteCount = sqlDB.delete(DroidGRRDatabase.TRANSFERS_TABLE, selection, selectionArgs);
				break;
			case TRANSFER_ID:
				String transferID = uri.getLastPathSegment();
				String transferWhereClause = Transfers._ID + " = " + transferID;
				if (!(TextUtils.isEmpty(selection))) {
					transferWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.TRANSFERS_TABLE, transferWhereClause, selectionArgs);
				break;
			case CONTACTS:
				deleteCount = sqlDB.delete(DroidGRRDatabase.CONTACTS_TABLE, selection, selectionArgs);
				break;
			case CONTACT_ID:
				String contactID = uri.getLastPathSegment();
				String contactWhereClause = Contacts.ID + " = " + contactID;
				if (!(TextUtils.isEmpty(selection))) {
					contactWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.CONTACTS_TABLE, contactWhereClause, selectionArgs);
				break;
			case CALENDAR:
				deleteCount = sqlDB.delete(DroidGRRDatabase.CALENDAR_TABLE, selection, selectionArgs);
				break;
			case CALENDAR_ID:
				String calendarID = uri.getLastPathSegment();
				String calendarWhereClause = Calendar.ID + " = " + calendarID;
				if (!(TextUtils.isEmpty(selection))) {
					calendarWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.CALENDAR_TABLE, calendarWhereClause, selectionArgs);
				break;
			case SMS:
				deleteCount = sqlDB.delete(DroidGRRDatabase.SMS_TABLE, selection, selectionArgs);
				break;
			case SMS_ID:
				String smsID = uri.getLastPathSegment();
				String smsWhereClause = Sms.MSG_ID + " = " + smsID;
				if (!(TextUtils.isEmpty(selection))) {
					smsWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.SMS_TABLE, smsWhereClause, selectionArgs);
				break;
			case MMS:
				deleteCount = sqlDB.delete(DroidGRRDatabase.MMS_TABLE, selection, selectionArgs);
				break;
			case MMS_ID:
				String mmsID = uri.getLastPathSegment();
				String mmsWhereClause = Mms.MSG_ID + " = " + mmsID;
				if (!(TextUtils.isEmpty(selection))) {
					mmsWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.MMS_TABLE, mmsWhereClause, selectionArgs);
				break;
			case PHOTOS:
				deleteCount = sqlDB.delete(DroidGRRDatabase.PHOTOS_TABLE, selection, selectionArgs);
				break;
			case PHOTO_ID:
				String photoID = uri.getLastPathSegment();
				String photoWhereClause = Photos.PHOTO_ID + " = " + photoID;
				if (!(TextUtils.isEmpty(selection))) {
					photoWhereClause += " AND " + selection;
				}
				deleteCount = sqlDB.delete(DroidGRRDatabase.PHOTOS_TABLE, photoWhereClause, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		if (deleteCount > 0) {
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return deleteCount;
	}

}
