package edu.ualr.jrming.droidgrr;

/**
 * DroidGRR.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

/** The main, launcher activity for DroidGRR - This is the user's entry point */
public class DroidGRR extends AppCompatActivity
        implements ConsentDialogFragment.consentDialogListener {

    private static final String TAG = "DroidGRR";
    private static boolean consentSelected = false;	//Whether or not the user has selected either
                                                    //option from the consent banner before.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            consentSelected = savedInstanceState.getBoolean("consentSelected");
        }

        setContentView(R.layout.activity_droidgrr);

        getFragmentManager().beginTransaction()
                .replace(R.id.frame1, new SettingsFragment())
                .replace(R.id.frame2, new WatcherButtonFragment())
                .commit();

        //Pop up consent dialog if the user hasn't already given consent.
        if (!consentSelected) {
            DialogFragment consentDialog = new ConsentDialogFragment();
            consentDialog.show(getSupportFragmentManager(), "ConsentDialogFragment");
        }

    }


    @Override
    public void onGiveConsent(DialogFragment dialog) {
        consentSelected = true;

        // User accepted the terms
        Log.v(TAG,"User Consent Banner - Accepted");

        // Check the preferences to see if watching is enabled
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPref.getBoolean(SettingsFragment.KEY_WATCHERS_ENABLED, true)) {
            // Start the service
            Intent serviceIntent = new Intent(getApplicationContext(), BackgroundWatcherService.class);
            startService(serviceIntent);
        } else {
            // Watching is disabled - ensure service is stopped and alert the user with a Snackbar
            Intent serviceIntent = new Intent(getApplicationContext(), BackgroundWatcherService.class);
            stopService(serviceIntent);

            //noinspection ConstantConditions
            Snackbar watcherSnackbar = Snackbar.make(findViewById(R.id.droidgrr_toplayout),
                    "Watching is disabled, no data will be collected", Snackbar.LENGTH_LONG);

            watcherSnackbar.setAction("Enable", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Update the preference to enable watchers
                    sharedPref.edit()
                            .putBoolean(SettingsFragment.KEY_WATCHERS_ENABLED, true)
                            .apply();

                    // Start the watcher service
                    Intent serviceIntent = new Intent(getApplicationContext(), BackgroundWatcherService.class);
                    startService(serviceIntent);
                }
            });
            watcherSnackbar.show();
        }
    }

    @Override
    public void onRefuseConsent(DialogFragment dialog) {
        consentSelected = true;

        // User rejected the terms
        Log.v(TAG,"User Consent Banner - Rejected");

        // Stop the service
        Intent serviceStopIntent = new Intent(getApplicationContext(), BackgroundWatcherService.class);
        stopService(serviceStopIntent);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Assume the user doesn't want to see the consent dialog again until they explicitly
        //destroy the app by pressing Back or by swiping it from Recent Apps, in which cases
        //onSaveInstanceState won't be called, and this won't be set.
        outState.putBoolean("consentSelected", true);
    }

}
