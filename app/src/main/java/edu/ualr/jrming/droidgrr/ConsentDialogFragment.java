package edu.ualr.jrming.droidgrr;

/**
 * ConsentDialogFragment.java
 * @author Jonathan Ming
 * Based on ConsentBanner.java, authored by Justin Grover
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * This file: Copyright 2016 Jonathan Ming
 * Basis file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/** This class is the Monitoring Consent DialogFragment which can be shown from any Activity. */
public class ConsentDialogFragment extends DialogFragment {

    public interface consentDialogListener {
        void onGiveConsent(DialogFragment dialog);
        void onRefuseConsent(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    consentDialogListener cListener;

    // Override the Fragment.onAttach() method to instantiate the consentDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the consentDialogListener so we can send events to the host
            cListener = (consentDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement consentDialogListener");
        }
    }

    @Override @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.user_consent_banner_text)
                .setPositiveButton(R.string.user_consent_accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        cListener.onGiveConsent(ConsentDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.user_consent_reject, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the negative button event back to the host activity
                        cListener.onRefuseConsent(ConsentDialogFragment.this);
                    }
                });
        return builder.create();
    }

}
