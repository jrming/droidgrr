package edu.ualr.jrming.droidgrr;

/**
 * CalendarWatcher.java
 * @author Jonathan Ming
 * Modified version of CalendarWatcher.java, authored by Justin Grover
 *
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;

import java.util.Date;

/** Retrieves calendar events and inserts them into the database in preparation for transfer */
public abstract class CalendarWatcher {

	private static final String TAG = "CalendarWatcher";

	/**
	 * This method retrieves and logs calendar events.
	 *
	 * @param context		The context from which to get the content resolver.
	 * @param startMillis	The oldest point in time from which to retrieve events.
	 * @param endMillis		The most recent point in time from which to retrieve events.
	 */
	public static void queryCalendar(Context context, long startMillis, long endMillis) {

		if (endMillis < 0) {
			// If a start time is provided, but no end time, then set the end time to right now.
			endMillis = System.currentTimeMillis();
		} else if (startMillis < 0) {
			// If an end time is provided, but no start time, then set the start time to 0,
			// which effectively gets all events until the end time
			startMillis = 0;
		}
		// If neither start nor end time is provided, then leave both invalid
		// to exclude the selection entirely and get ALL events, ever (see line 72)

		// Initialize the Android calendar content provider URI
		Uri calUri = CalendarContract.EventsEntity.CONTENT_URI;

		// Query calendar for events since the given time
		String[] calProj = {
				CalendarContract.EventsEntity.DTSTART,
				CalendarContract.EventsEntity.TITLE,
				CalendarContract.EventsEntity._ID
		};

		String calSel = null;
		String[] calSelArgs = null;
		// If start or end time is invalid, then omit the selection clause entirely
		if(startMillis > 0 && endMillis > 0) {
			calSel = CalendarContract.EventsEntity.DTSTART + " BETWEEN ? AND ?";
			calSelArgs = new String[]{ Long.toString(startMillis), Long.toString(endMillis) };
		}

		Cursor calCursor;
		try {
			calCursor = context.getContentResolver().query(calUri, calProj, calSel, calSelArgs, null);
		}
		catch(Exception e) {
			Log.e(TAG, "Unable to query "+calUri+": "+e.getMessage());
			return;
		}
		
		// Parse query results
		if (calCursor == null) {
			Log.e(TAG, "Unable to query "+calUri);
			return;
		}
		
		if (calCursor.getCount() > 0) {
        	String title;
        	Date date;
        	long id;
        	while(calCursor.moveToNext()) {
        		// Get event details
        		title = calCursor.getString(
						calCursor.getColumnIndexOrThrow(CalendarContract.EventsEntity.TITLE)
				);
        		date = new Date(calCursor.getLong(
						calCursor.getColumnIndexOrThrow(CalendarContract.EventsEntity.DTSTART)
				));
        		id = calCursor.getLong(
						calCursor.getColumnIndexOrThrow(CalendarContract.EventsEntity._ID)
				);
        		
        		// Check to see if this event is already in the DroidGRR calendar database
        		String[] projection = { DroidGRRProvider.Calendar.ID };
                String selection = DroidGRRProvider.Calendar.ID + " = ?";
				String[] selectionArgs = { Long.toString(id) };
                Cursor cursor;
                try {
                	cursor = context.getContentResolver().query(
							DroidGRRProvider.Calendar.CONTENT_URI,
							projection,
							selection,
							selectionArgs,
							null);
                }
                catch(Exception e) {
                	Log.e(TAG, "Error querying calendar table in results.db: "+e.getMessage());
                	return;
                }
                
                if (cursor == null) {
                	Log.e(TAG, "Error querying calendar table in results.db");
                	return;
                }
                	
                if (cursor.getCount() == 0) {
            		// Get current time
            		long currentTime = System.currentTimeMillis();

            		// Log calendar event in calendar table
            		ContentValues calendarValues = new ContentValues();
            		calendarValues.put(DroidGRRProvider.Calendar.ID, id);
            		calendarValues.put(DroidGRRProvider.Calendar.DATE_ADDED, currentTime);
            		calendarValues.put(DroidGRRProvider.Calendar.NAME, title);
            		calendarValues.put(DroidGRRProvider.Calendar.EVENT_DATE, date.getTime());
            		context.getContentResolver().insert(DroidGRRProvider.Calendar.CONTENT_URI, calendarValues);
				}
				// Close this DroidGRRProvider Cursor, next time will have a new query anyway
				cursor.close();
        	}
		}
		// Done with the Cursor, close it
		calCursor.close();
	}

}
