package edu.ualr.jrming.droidgrr;

/**
 * WatcherIntentService.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling Watcher tasks asynchronously in
 * a separate thread
 */
public class WatcherIntentService extends IntentService {
    private static final String TAG = "WatcherIntentService";

    private static final String ACTION_RETRIEVE_ACCOUNTS = "edu.ualr.jrming.droidgrr.action.RETRIEVE_ACCOUNTS";
    private static final String ACTION_RETRIEVE_CALENDAR = "edu.ualr.jrming.droidgrr.action.RETRIEVE_CALENDAR";
    private static final String ACTION_RETRIEVE_CALL_LOG = "edu.ualr.jrming.droidgrr.action.RETRIEVE_CALL_LOG";
    private static final String ACTION_RETRIEVE_SMS_LOG = "edu.ualr.jrming.droidgrr.action.RETRIEVE_SMS_LOG";
    private static final String ACTION_RETRIEVE_MMS_LOG = "edu.ualr.jrming.droidgrr.action.RETRIEVE_MMS_LOG";
    private static final String ACTION_RETRIEVE_CONTACTS = "edu.ualr.jrming.droidgrr.action.RETRIEVE_CONTACTS";
    private static final String ACTION_RETRIEVE_PHOTOS = "edu.ualr.jrming.droidgrr.action.RETRIEVE_PHOTOS";
    private static final String ACTION_RETRIEVE_LOCATION = "edu.ualr.jrming.droidgrr.action.RETRIEVE_LOCATION";
    private static final String ACTION_RETRIEVE_LOGCAT = "edu.ualr.jrming.droidgrr.action.RETRIEVE_LOGCAT";

    private static final String START_MILLIS = "edu.ualr.jrming.droidgrr.extra.START_MILLIS";
    private static final String END_MILLIS = "edu.ualr.jrming.droidgrr.extra.END_MILLIS";

    public WatcherIntentService() {
        super("WatcherIntentService");
    }


    /**
     * Retrieves device accounts from DeviceAccountWatcher and stores them into the database
     * @param context   The context in which to deliver the intent
     */
    public static void queryDeviceAccounts(Context context) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_ACCOUNTS);
        context.startService(intent);
    }

    /**
     * Retrieves calendar events from CalendarWatcher and stores them into the database
     * in preparation for transfer.
     * @param context       The context in which to deliver the intent
     * @param startMillis    The oldest point in time from which to retrieve events.
     * @param endMillis        The most recent point in time from which to retrieve events.
     */
    public static void queryCalendar(Context context, long startMillis, long endMillis) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_CALENDAR);
        intent.putExtra(START_MILLIS, startMillis);
        intent.putExtra(END_MILLIS, endMillis);
        context.startService(intent);
    }

    /**
     * Retrieves call log from CallLogWatcher and stores it into the database
     * @param context       The context in which to deliver the intent
     * @param startMillis    The oldest point in time from which to retrieve events.
     * @param endMillis        The most recent point in time from which to retrieve events.
     */
    public static void queryCallLog(Context context, long startMillis, long endMillis) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_CALL_LOG);
        intent.putExtra(START_MILLIS, startMillis);
        intent.putExtra(END_MILLIS, endMillis);
        context.startService(intent);
    }

    /**
     * Retrieves SMS log from SMSWatcher and stores it into the database
     * @param context       The context in which to deliver the intent
     * @param startMillis   The oldest point in time from which to retrieve events.
     * @param endMillis     The most recent point in time from which to retrieve events.
     */
    public static void querySmsLog(Context context, long startMillis, long endMillis) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_SMS_LOG);
        intent.putExtra(START_MILLIS, startMillis);
        intent.putExtra(END_MILLIS, endMillis);
        context.startService(intent);
    }

    /**
     * Retrieves MMS log from MMSWatcher and stores it into the database
     * @param context       The context in which to deliver the intent
     * @param startMillis   The oldest point in time from which to retrieve events.
     * @param endMillis     The most recent point in time from which to retrieve events.
     */
    public static void queryMmsLog(Context context, long startMillis, long endMillis) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_MMS_LOG);
        intent.putExtra(START_MILLIS, startMillis);
        intent.putExtra(END_MILLIS, endMillis);
        context.startService(intent);
    }

    /**
     * Retrieves contacts from ContactWatcher and stores them into the database
     * @param context   The context in which to deliver the intent
     */
    public static void queryContacts(Context context) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_CONTACTS);
        context.startService(intent);
    }

    /**
     * Retrieves image metadata from PhotoWatcher and stores it into the database
     * @param context       The context in which to deliver the intent
     * @param startMillis   The oldest point in time from which to retrieve events.
     * @param endMillis     The most recent point in time from which to retrieve events.
     */
    public static void queryPhotos(Context context, long startMillis, long endMillis) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_PHOTOS);
        intent.putExtra(START_MILLIS, startMillis);
        intent.putExtra(END_MILLIS, endMillis);
        context.startService(intent);
    }

    /**
     * Retrieves the device's current location from LocationWatcher and stores it into the database
     * @param context       The context in which to deliver the intent
     */
    public static void queryLocation(Context context) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_LOCATION);
        context.startService(intent);
    }

    /**
     * Retrieves system logs from LogcatWatcher and stores it into the database
     * @param context       The context in which to deliver the intent
     */
    public static void queryLogcat(Context context) {
        Intent intent = new Intent(context, WatcherIntentService.class);
        intent.setAction(ACTION_RETRIEVE_LOGCAT);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            Log.v(TAG, "Handling action " + action);
            final long startMillis, endMillis;
            switch (action) {
                case ACTION_RETRIEVE_ACCOUNTS:
                    DeviceAccountWatcher.findAccounts(this);
                    break;
                case ACTION_RETRIEVE_CALENDAR:
                    startMillis = intent.getLongExtra(START_MILLIS, -1);
                    endMillis = intent.getLongExtra(END_MILLIS, -1);
                    CalendarWatcher.queryCalendar(this, startMillis, endMillis);
                    break;
                case ACTION_RETRIEVE_CALL_LOG:
                    startMillis = intent.getLongExtra(START_MILLIS, -1);
                    endMillis = intent.getLongExtra(END_MILLIS, -1);
                    CallLogWatcher.queryCallLog(this, startMillis, endMillis);
                    break;
                case ACTION_RETRIEVE_SMS_LOG:
                    startMillis = intent.getLongExtra(START_MILLIS, -1);
                    endMillis = intent.getLongExtra(END_MILLIS, -1);
                    SMSWatcher.querySMSLog(this, startMillis, endMillis);
                    break;
                case ACTION_RETRIEVE_MMS_LOG:
                    startMillis = intent.getLongExtra(START_MILLIS, -1);
                    endMillis = intent.getLongExtra(END_MILLIS, -1);
                    MMSWatcher.queryMMSLog(this, startMillis, endMillis);
                    break;
                case ACTION_RETRIEVE_CONTACTS:
                    ContactWatcher.queryContacts(this);
                    break;
                case ACTION_RETRIEVE_PHOTOS:
                    startMillis = intent.getLongExtra(START_MILLIS, -1);
                    endMillis = intent.getLongExtra(END_MILLIS, -1);
                    PhotoWatcher.queryPhotos(this, startMillis, endMillis);
                    break;
                case ACTION_RETRIEVE_LOCATION:
                    (new LocationWatcher(this)).queryLocation();
                    break;
                case ACTION_RETRIEVE_LOGCAT:
                    LogcatWatcher.retrieveLogs(this);
                    break;
                default:
                    break;
            }
        }
    }

}
