package edu.ualr.jrming.droidgrr;

/**
 * CipherUtils.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.util.Log;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * A port of the grr/lib/communicator.py > Cipher class.
 * A utility class for storing cipher configurations and handling message encryption & decryption.
 */
public class CipherUtils { //TODO: All the error handling in this class needs work

    private static final String TAG = "CipherUtils";
    private static final String CIPHER_NAME = "aes_128_cbc";
    public static final int KEY_SIZE = 128; // In bits
    public static final int IV_SIZE = 128;  // In bits

    protected SecretKey mCipherKey;
    protected SecretKey mHmacKey;

    public JobProtos.CipherProperties mCipherProps;
    public JobProtos.CipherMetadata mCipherMetadata;

    protected byte[] mEncryptedCipher;
    protected byte[] mEncryptedCipherMetadata;

    /**
     * For generating new Cipher data for an outbound communication.
     * See grr/lib/communicator.py > Cipher
     */
    public CipherUtils(String sourceCommonName, PrivateKey privateKey, PublicKey remotePublicKey) {
        // The Cipher and HMAC keys will both be needed in the encrypt/decrypt methods, so they're
        // stored as member variables. The metadata IV, however, should be stored and forgotten, so
        // it's in a local variable here.
        SecretKey metadataIv;
        try {
            KeyGenerator aesKg = KeyGenerator.getInstance("AES");
            KeyGenerator hmacKg = KeyGenerator.getInstance("HmacSHA1");
            aesKg.init(KEY_SIZE);
            hmacKg.init(KEY_SIZE);

            mCipherKey = aesKg.generateKey();
            metadataIv = aesKg.generateKey();   // Right now, GRR uses an IV and Key of equal size
            mHmacKey = hmacKg.generateKey();
        } catch (NoSuchAlgorithmException e) {
            Log.wtf(TAG, "Apparently Java no longer supports AES key generation.");
            throw new RuntimeException("CipherUtils:87 - KeyGenerator Algorithm Not Found");
        }

        // Construct the CipherProperties proto
        mCipherProps = JobProtos.CipherProperties.newBuilder()
                .setName(CIPHER_NAME)
                .setKey(ByteString.copyFrom(mCipherKey.getEncoded()))
                .setMetadataIv(ByteString.copyFrom(metadataIv.getEncoded()))
                .setHmacKey(ByteString.copyFrom(mHmacKey.getEncoded()))
                .setHmacType(JobProtos.CipherProperties.HMACType.FULL_HMAC)
                .build();
        byte[] cipherPropsBytes = mCipherProps.toByteArray();

        // Create a Builder for the CipherMetadata proto, storing the source's common name
        JobProtos.CipherMetadata.Builder cipherMetadataBuilder = JobProtos.CipherMetadata.newBuilder()
                .setSource(sourceCommonName);

        // Attempt to store cipherProps, serialized and signed with our private key, to the metadata
        cipherMetadataBuilder.setSignature(signCipher(privateKey));

        // Build the completed CipherMetadata proto
        mCipherMetadata = cipherMetadataBuilder.build();

        try {
            // Next, encrypt cipherProps with the server's RSA public key
            Cipher rsaCipher = Cipher.getInstance("RSA/NONE/OAEPwithSHA-1andMGF1Padding");
            rsaCipher.init(Cipher.ENCRYPT_MODE, remotePublicKey);
            mEncryptedCipher = rsaCipher.doFinal(cipherPropsBytes);

            // Finally, encrypt cipherMetadata symetrically with AES
            mEncryptedCipherMetadata = encrypt( mCipherMetadata,
                                                mCipherProps.getMetadataIv().toByteArray() );
        } catch (Exception e) {
            Log.e(TAG, "Error encrypting cipher data.");
            throw new RuntimeException("CipherUtils:121 - Cipher RSA Encryption Failure");
        }
    } // End outbound-comm constructor

    /**
     * For deriving Cipher data from a received communication.
     * See grr/lib/communicator.py > ReceivedCipher
     */
    public CipherUtils(JobProtos.ClientCommunication receivedComm, PrivateKey privateKey)
            throws GeneralSecurityException {

        if (receivedComm.getApiVersion() != 3) {
            throw new UnsupportedClassVersionError("Unsupported GRR API version, expected 3.");
        }
        if (!receivedComm.hasEncryptedCipher()) {
            throw new GeneralSecurityException("Server response is not encrypted.");
        }

        // Try to decrypt the cipher key, IV, and HMAC key
        try {
            mEncryptedCipher = receivedComm.getEncryptedCipher().toByteArray();
            mEncryptedCipherMetadata = receivedComm.getEncryptedCipherMetadata().toByteArray();

            Cipher rsaCipher = Cipher.getInstance("RSA/NONE/OAEPwithSHA-1andMGF1Padding");
            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);

            // Store the decrypted value into cipherProps so we can access the individual properties
            mCipherProps = JobProtos.CipherProperties.parseFrom(
                    rsaCipher.doFinal(mEncryptedCipher)
            );

        } catch (Exception e) {
            Log.e(TAG, "Error decrypting cipher data.");
            throw new RuntimeException("CipherUtils:154 - Cipher RSA Decryption Failure");
        }


        // Verify the key lengths
        if (   mCipherProps.getKey().size() != KEY_SIZE / 8
                || mCipherProps.getMetadataIv().size() != IV_SIZE / 8
                || mCipherProps.getHmacKey().size() != KEY_SIZE / 8) {

            throw new GeneralSecurityException("Received cipher is invalid.");
        }
        // If we made it here, key lengths are valid. Assign them.
        mCipherKey = new SecretKeySpec(mCipherProps.getKey().toByteArray(), "AES");
        mHmacKey = new SecretKeySpec(mCipherProps.getHmacKey().toByteArray(), "HmacSHA1");

        // Verify the full HMAC
        byte[] hashBytesMissingApi = org.spongycastle.util.Arrays.concatenate(
                receivedComm.getEncrypted().toByteArray(),
                receivedComm.getEncryptedCipher().toByteArray(),
                receivedComm.getEncryptedCipherMetadata().toByteArray(),
                receivedComm.getPacketIv().toByteArray()
        );
        byte[] apiBytes = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
                .putInt( receivedComm.getApiVersion() )
                .array();

        byte[] fullHmac = hmac(
                org.spongycastle.util.Arrays.concatenate(hashBytesMissingApi, apiBytes)
        );

        // Our calculated HMAC and the HMAC in the received comm should be the same.
        if (!Arrays.equals(fullHmac, receivedComm.getFullHmac().toByteArray())) {
            // If they're not the same, we've got a problem:
            throw new GeneralSecurityException("HMAC verification failed.");
        }


        // Decrypt the cipher metadata using the keys we've just extracted
        try {
            mCipherMetadata = JobProtos.CipherMetadata.parseFrom(
                    decrypt(mEncryptedCipherMetadata, mCipherProps.getMetadataIv().toByteArray())
            );
        } catch (InvalidProtocolBufferException e) {
            throw new GeneralSecurityException("Invalid CipherMetadata format.");
        }
    } //End received-comm constructor


    /**
     * Signs the serialized contents of mCipherProps and returns the resulting signature.
     * @param privateKey The private key with which to sign
     * @return The signature as a ByteString
     */
    private ByteString signCipher(PrivateKey privateKey) {
        try {
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initSign(privateKey);
            sig.update(mCipherProps.toByteArray());

            return ByteString.copyFrom( sig.sign() );

        } catch (NoSuchAlgorithmException e) {
            Log.wtf(TAG, "Apparently Java no longer supports SHA256 RSA signatures.");
            throw new RuntimeException("CipherUtils:217 - Signature Algorithm Not Found");
        } catch (InvalidKeyException e) {
            Log.e(TAG, "Invalid private key provided to CipherUtils constructor!");
            throw new IllegalArgumentException("Invalid private key.");
        } catch (SignatureException e) {
            Log.e(TAG, e.getLocalizedMessage());
            throw new RuntimeException("CipherUtils:223 - Signature Exception");
        }
    }

    /**
     * Verifies the signature stored in mCipherMetadata against the given public key.
     * @param publicKey The public key with which to perform the verification.
     * @return {@literal true} if the the signature was verified; {@literal false} otherwise.
     */
    public boolean verifyCipherSignature(PublicKey publicKey) {
        // First try the default - PKCS1v1.5 padding - but if that doesn't work, try PSS
        return verifyCipherSignature(publicKey, "SHA256withRSA")
            || verifyCipherSignature(publicKey, "SHA256withRSA/PSS");
    }
    private boolean verifyCipherSignature(PublicKey publicKey, String algorithm) {
        try {
            Signature sig = Signature.getInstance(algorithm);
            sig.initVerify(publicKey);
            sig.update(mCipherProps.toByteArray());
            return sig.verify( mCipherMetadata.getSignature().toByteArray() );
        } catch (NoSuchAlgorithmException e) {
            Log.wtf(TAG, "Apparently the provider no longer supports "+algorithm+" signatures.");
            throw new RuntimeException("CipherUtils:249 - Signature Algorithm Not Found");
        } catch (InvalidKeyException e) {
            Log.e(TAG, "Invalid public key provided to verifyCipherSignature");
            throw new IllegalArgumentException("Invalid public key.");
        } catch (SignatureException e) {
            Log.e(TAG, e.getLocalizedMessage());
            throw new RuntimeException("CipherUtils:255 - Signature Exception");
        }
    }

    /**
     * Encrypts a given protocol buffer message by calling toByteArray and encrypting the bytes
     * via AES128 CBC with PKCS7 padding. Requires the caller to provide an initialization vector.
     * @param msg The protocol buffer message (already built) to serialize and encrypt
     * @param iv The initialization vector, preferably obtained by creating a SecretKey and passing
     *           the output of that key's getEncoded() function.
     * @return The encrypted message as a byte array
     */
    public byte[] encrypt(Message msg, byte[] iv) {
        try {
            Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivParam = new IvParameterSpec(iv);
            aesCipher.init(Cipher.ENCRYPT_MODE, mCipherKey, ivParam);
            return aesCipher.doFinal( msg.toByteArray() );

        } catch (GeneralSecurityException e) {
            Log.wtf(TAG, "Unable to encrypt. " + e.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Decrypts an object encrypted using the encrypt() method of this class. Requires the caller
     * to pass in the same initialization vector provided to encrypt().
     * @param encrypted The encrypted data as a byte array
     * @param iv The initialization vector used to encrypt this data
     * @return The decrypted data as a byte array
     */
    public byte[] decrypt(byte[] encrypted, byte[] iv) {
        try {
            Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivParam = new IvParameterSpec(iv);
            aesCipher.init(Cipher.DECRYPT_MODE, mCipherKey, ivParam);
            return aesCipher.doFinal(encrypted);

        } catch (GeneralSecurityException e) {
            Log.e(TAG, "Unable to decrypt. " + e.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Calculates the SHA1 HMAC for an arbitrary sequence of bytes, returning the result.
     * @param data The bytes to hash with SHA1 (either as byte array or comma-separated)
     * @return The HMAC result
     */
    public byte[] hmac(byte... data) {
        try {
            Mac hmacGen = Mac.getInstance("HmacSHA1");
            hmacGen.init(mHmacKey);
            return hmacGen.doFinal(data);

        } catch (GeneralSecurityException e) {
            Log.e(TAG, "Unable to hash. " + e.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Generates a new IV_SIZE long AES SecretKey, in raw byte format, fit to serve as an IV
     * @return A byte array fit to use as an AES initialization vector
     */
    public byte[] createNewIv() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(IV_SIZE);

            return keyGen.generateKey().getEncoded();

        } catch (NoSuchAlgorithmException e) {
            Log.wtf(TAG, "Apparently Java no longer supports generating AES keys.");
            return null;
        }
    }


    public byte[] getEncryptedCipher() {
        return Arrays.copyOf(mEncryptedCipher, mEncryptedCipher.length);
    }

    public byte[] getEncryptedCipherMetadata() {
        return Arrays.copyOf(mEncryptedCipherMetadata, mEncryptedCipherMetadata.length);
    }
}
