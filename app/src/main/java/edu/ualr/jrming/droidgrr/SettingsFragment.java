package edu.ualr.jrming.droidgrr;

/**
 * SettingsFragment.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

/** A PreferenceFragment containing the basic settings for DroidGRR */
public class SettingsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener,
                   Preference.OnPreferenceClickListener {

    public static String KEY_WATCHERS_ENABLED = "watcher_switch";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_settings);
        setHasOptionsMenu(true);

        findPreference("start_enroll").setOnPreferenceClickListener(this);
        findPreference("start_poll").setOnPreferenceClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            ListView listView = (ListView) getView().findViewById(android.R.id.list);
            Adapter adapter = listView.getAdapter();

            if (adapter != null) {
                int height = 0;
                //int height = listView.getPaddingTop() + listView.getPaddingBottom();
                for (int i = 0; i < adapter.getCount(); i++) {
                    View item = adapter.getView(i, null, listView);
                    item.measure(0, 0);
                    height += item.getMeasuredHeight();
                }

                FrameLayout frame = (FrameLayout) getActivity().findViewById(R.id.frame1);
                ViewGroup.LayoutParams param = frame.getLayoutParams();
                param.height = height + (listView.getDividerHeight() * adapter.getCount());
                frame.setLayoutParams(param);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Register preferences listener
        PreferenceManager.getDefaultSharedPreferences(getActivity())
                .registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(KEY_WATCHERS_ENABLED)) {
            SwitchPreference watcherSwitch = (SwitchPreference) findPreference(key);
            // Start the service if pref was enabled, otherwise stop it
            if (prefs.getBoolean(key, true)) {
                if (watcherSwitch != null) {
                    watcherSwitch.setChecked(true);
                }
                Intent serviceIntent = new Intent(getActivity().getApplicationContext(), BackgroundWatcherService.class);
                getActivity().startService(serviceIntent);
            } else {
                if (watcherSwitch != null) {
                    watcherSwitch.setChecked(false);
                }
                // Stop the service
                Intent serviceIntent = new Intent(getActivity().getApplicationContext(), BackgroundWatcherService.class);
                getActivity().stopService(serviceIntent);
            }
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        switch(preference.getKey()) {
            case "start_enroll":
                preference.setSummary("Attempting to enroll client...");
                try {
                    (new EnrollClient(getActivity(), preference)).execute();
                    return true;
                } catch (Exception e) {
                    Log.e("SettingsFragment", e.getMessage());
                    Toast.makeText(getActivity(), "Error enrolling client!", Toast.LENGTH_LONG).show();
                }
                break;

            case "start_poll":
                Toast.makeText(getActivity(), "Attempting to poll server", Toast.LENGTH_SHORT).show();
                Communicator.pollOnce(getActivity());
        }
        return false;
    }


    @Override
    public void onPause() {
        // Unregister preferences listener
        PreferenceManager.getDefaultSharedPreferences(getActivity())
                .unregisterOnSharedPreferenceChangeListener(this);

        super.onPause();
    }
}