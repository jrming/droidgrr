package edu.ualr.jrming.droidgrr;

/**
 * CallLogWatcher.java
 * @author Jonathan Ming
 * Modified version of CallLogWatcher.java, authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog.Calls;
import android.util.Log;

import java.util.Date;

/** Retrieves the call log and stores it into the database in preparation for transfer */
public abstract class CallLogWatcher {

	private static final String TAG = "CallWatcher";

    /** Query the call log database for new calls */
    public static void queryCallLog(Context context, long startMillis, long endMillis) {

		if (endMillis < 0) {
			// If a start time is provided, but no end time, then set the end time to right now.
			endMillis = System.currentTimeMillis();
		} else if (startMillis < 0) {
			// If an end time is provided, but no start time, then set the start time to 0,
			// which effectively gets all events until the end time
			startMillis = 0;
		}
		// If neither start nor end time is provided, then leave both invalid
		// to exclude the selection entirely and get ALL events, ever (see line 72)

		// Query the the call database
    	Cursor callCursor;
    	String[] callProj = {
				Calls._ID,
				Calls.NUMBER,
				Calls.CACHED_NAME,
				Calls.DURATION,
				Calls.DATE,
				Calls.CACHED_NUMBER_TYPE,
				Calls.TYPE
		};

		String callSel = null;
		String[] callSelArgs = null;
		// If start or end time is invalid, then omit the selection clause entirely
		if(startMillis > 0 && endMillis > 0) {
			callSel = Calls.DATE + " BETWEEN ? AND ?";
			callSelArgs = new String[]{ Long.toString(startMillis), Long.toString(endMillis) };
		}
		String callSort = Calls.DATE + " DESC";

    	try {
    		callCursor = context.getContentResolver().query(Calls.CONTENT_URI, callProj, callSel, callSelArgs, callSort);
    	}
		catch(SecurityException e) {
			Log.e(TAG, "Permission denied to access CallLog content provider");
			return;
		}
    	catch(Exception e) {
    		Log.e(TAG, "Unable to query CallLog content provider");
    		return;
    	}
    	if (callCursor == null) {
    		Log.e(TAG, "Unable to query CallLog content provider");
    		return;
    	}
    	
		if (callCursor.getCount() > 0) {
			// Initialize DroidWatch events content provider URI
			Uri eventsUri = DroidGRRProvider.Events.CONTENT_URI;

			int id;
			String contactNumber;
			String contactName;
			long duration;
			Date callDate;
			String numType;
			int callType;

			while(callCursor.moveToNext()) {
				// Get call event information
				id = callCursor.getInt(callCursor.getColumnIndexOrThrow(Calls._ID));
				contactNumber = callCursor.getString(callCursor.getColumnIndexOrThrow(Calls.NUMBER));
				contactName = callCursor.getString(callCursor.getColumnIndexOrThrow(Calls.CACHED_NAME));
				duration = callCursor.getLong(callCursor.getColumnIndexOrThrow(Calls.DURATION));
				callDate = new Date(callCursor.getLong(callCursor.getColumnIndexOrThrow(Calls.DATE)));
				numType = callCursor.getString(callCursor.getColumnIndexOrThrow(Calls.CACHED_NUMBER_TYPE));
				callType = callCursor.getInt(callCursor.getColumnIndexOrThrow(Calls.TYPE));

				// Determine call direction
				String direction;
				if (callType == Calls.INCOMING_TYPE) {
					direction = "Incoming";
				} else if (callType == Calls.MISSED_TYPE) {
					direction = "Incoming - Missed";
				} else {
					direction = "Outgoing";
				}

				// Check to see if this call is already in the database
				String[] projection = { DroidGRRDatabase.DETECTOR_COLUMN, DroidGRRDatabase.ADDITIONAL_INFO_COLUMN };
				String selection = DroidGRRDatabase.DETECTOR_COLUMN + " = ? AND "
						+ DroidGRRDatabase.ADDITIONAL_INFO_COLUMN + " LIKE ?";
				String[] selectionArgs = { TAG, "%ID:"+id+";%" };
				Cursor cursor;
				try {
					cursor = context.getContentResolver().query(eventsUri, projection, selection, selectionArgs, null);
				} catch (Exception e) {
					Log.e(TAG, "Unable to query events table: " + e.getMessage());
					return;
				}
				if (cursor == null) {
					Log.e(TAG, "Unable to query events table");
					return;
				}

				if (cursor.getCount() == 0) {
					// Insert phone call event into DroidWatch
					ContentValues values = new ContentValues();
					values.put(DroidGRRDatabase.DETECTOR_COLUMN, TAG);
					values.put(DroidGRRDatabase.EVENT_ACTION_COLUMN, "Phone Call");
					values.put(DroidGRRDatabase.EVENT_DATE_COLUMN, callDate.getTime());
					values.put(DroidGRRDatabase.EVENT_DESCRIPTION_COLUMN, direction);
					values.put(DroidGRRDatabase.ADDITIONAL_INFO_COLUMN, "ID:" + id + "; Number:" + contactNumber + "; Name:" + contactName + "; Duration:" + duration + " NumType:" + numType + ";");
					context.getContentResolver().insert(eventsUri, values);
				}
				// Close this DroidGRRProvider Cursor, next time will have a new query anyway
				cursor.close();
			}
		}
		// Done with the cursor, close it
		callCursor.close();
    }
}
