package edu.ualr.jrming.droidgrr;

/**
 * SMSWatcher.java
 * @author Jonathan Ming
 * Based on SMSIncomingWatcher.java and SMSOutgoingWatcher.java, both authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 *
 * This file: Copyright 2016 Jonathan Ming
 * Basis files: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.util.Log;

import java.util.Date;

/** This class retrieves sent and received SMS messages. **/
public abstract class SMSWatcher  {

	private static final String TAG = "SMSWatcher";
    
    /**
	 * Retrieve SMS messages, using the supported API if on API 19 or higher and resorting
	 * to the unsupported content:/sms/ provider for older versions
     */
    public static void querySMSLog(Context context, long startMillis, long endMillis) {

		if (endMillis < 0) {
			// If a start time is provided, but no end time, then set the end time to right now.
			endMillis = System.currentTimeMillis();
		} else if (startMillis < 0) {
			// If an end time is provided, but no start time, then set the start time to 0,
			// which effectively gets all events until the end time
			startMillis = 0;
		}
		// If neither start nor end time is provided, then leave both invalid
		// to exclude the selection entirely and get ALL events, ever (see line 72)

		if(Build.VERSION.SDK_INT >= 19) {
			supportedSmsQuery(context, startMillis, endMillis);
		} else {
			unsupportedSmsQuery(context, startMillis, endMillis);
		}
    }

	@TargetApi(19)
	private static void supportedSmsQuery(Context context, long startMillis, long endMillis) {
		Log.d(TAG, "Hit supportedSmsQuery");
		// Initialize API-supported SMS content provider
		Uri smsUri = Telephony.Sms.CONTENT_URI;

		// Query for message id, address, sender (person), date received, type, and body
		String[] smsProj = {
				Telephony.Sms._ID,
				Telephony.Sms.THREAD_ID,
				Telephony.Sms.ADDRESS,
				Telephony.Sms.PERSON,
				Telephony.Sms.DATE,
				Telephony.Sms.TYPE,
				Telephony.Sms.BODY
		};
		String smsSel = null;
		String[] smsSelArgs = null;
		if (startMillis > 0 && endMillis > 0) {
			smsSel = Telephony.Sms.DATE + " BETWEEN ? AND ?";
			smsSelArgs = new String[]{ Long.toString(startMillis), Long.toString(endMillis) };
		}
		String smsOrder = Telephony.Sms.DATE + " DESC";

		Cursor smsCursor;
		try {
			smsCursor = context.getContentResolver().query(smsUri, smsProj, smsSel, smsSelArgs, smsOrder);
		} catch(Exception e) {
			Log.e(TAG, "Unable to query supported SMS Content Provider");
			return;
		}
		if(smsCursor == null) {
			Log.e(TAG, "Supported SMS Content Provider returned null cursor");
			return;
		}

		readCursor(context, smsCursor);
	}

	@TargetApi(14)
	private static void unsupportedSmsQuery(Context context, long startMillis, long endMillis) {
		Log.d(TAG, "Hit unsupportedSmsQuery");
		// Initialize SMS content provider URI (undocumented)
		Uri smsUri = Uri.parse("content://sms");

		// Query for message id, address, sender (person), date received, type, and body
		String[] smsProj = {
				"_id",
				"thread_id",
				"address",
				"person",
				"date",
				"type",
				"body"
		};
		String smsSel = null;
		String[] smsSelArgs = null;
		if (startMillis > 0 && endMillis > 0) {
			smsSel = "date BETWEEN ? AND ?";
			smsSelArgs = new String[]{ Long.toString(startMillis), Long.toString(endMillis) };
		}
		String smsOrder = "date DESC";
		Cursor smsCursor;
		try {
			smsCursor = context.getContentResolver().query(smsUri, smsProj, smsSel, smsSelArgs, smsOrder);
		} catch(Exception e) {
			Log.e(TAG, "Unable to query URI: "+smsUri.toString());
			return;
		}
		if (smsCursor == null) {
			Log.e(TAG, "Unsupported SMS content provider returned null cursor");
			return;
		}

		readCursor(context, smsCursor);
	}


	private static void readCursor(Context context, Cursor smsCursor) {
		if (smsCursor.getCount() > 0) {
			long messageID;
			int threadID;
			String to;
			int from;
			Date date;
			int smsType;
			String body;

			while(smsCursor.moveToNext()) {
				// Get SMS message
				try {
					messageID	= smsCursor.getLong( smsCursor.getColumnIndexOrThrow("_id") );
					threadID	= smsCursor.getInt( smsCursor.getColumnIndexOrThrow("thread_id") );
					to			= smsCursor.getString( smsCursor.getColumnIndexOrThrow("address") );
					from		= smsCursor.getInt( smsCursor.getColumnIndexOrThrow("person") );
					date		= new Date( smsCursor.getLong( smsCursor.getColumnIndexOrThrow("date") ) );
					smsType		= smsCursor.getInt( smsCursor.getColumnIndexOrThrow("type") );
					body		= smsCursor.getString( smsCursor.getColumnIndexOrThrow("body") );
				} catch (Exception e) {
					Log.e(TAG, "Unable to retrieve SMS fields");
					return;
				}
				if (messageID == -1) {
					Log.e(TAG, "Cursor returned invalid SMS");
					return;
				}

				// Parse SMS type
				String smsAction;
				String contact = null;
				switch(smsType) {
					case 1:
						smsAction = "Received";
						// Get contact name using From contact
						contact = ContactFinder.findContactById(context, from);
						break;
					case 2:
					case 4:
						smsAction = "Sent";
						break;
					case 5:
						smsAction = "Failed to send";
						break;
					case 6:
						smsAction = "Queued to send";
						break;
					default:
						// We don't want any type besides the ones above - skip to the next message
						continue;
				}

				// If the contact name wasn't set via "Received", then set it to the address contact
				if (contact == null) {
					contact = ContactFinder.findContactByAddress(context, to);
				}


				// Check the DroidWatch content provider to make sure message is not a duplicate
				Uri eventsUri = DroidGRRProvider.Sms.CONTENT_URI;
				String[] projection = { DroidGRRProvider.Sms.MSG_ID};
				String selection = DroidGRRProvider.Sms.MSG_ID + " = ?";
				String[] selectionArgs = { Long.toString(messageID) };
				Cursor cursor;
				try {
					cursor = context.getContentResolver().query(eventsUri, projection, selection, selectionArgs, null);
				} catch (Exception e) {
					Log.e(TAG, "Unable to query events table");
					return;
				}
				if (cursor == null) {
					Log.e(TAG, "DroidGRR Events Content Provider returned null cursor");
					return;
				}

				if (cursor.getCount() == 0) {
					// Insert new SMS into DroidGRR
					ContentValues values = new ContentValues();
					values.put(DroidGRRProvider.Sms.MSG_ID, messageID);
					values.put(DroidGRRProvider.Sms.THREAD_ID, threadID);
					values.put(DroidGRRProvider.Sms.ADDRESS, to);
					values.put(DroidGRRProvider.Sms.CONTACT, contact);
					values.put(DroidGRRProvider.Sms.DATE, date.getTime());
					values.put(DroidGRRProvider.Sms.TYPE, smsAction);
					values.put(DroidGRRProvider.Sms.BODY, body);
					context.getContentResolver().insert(DroidGRRProvider.Sms.CONTENT_URI, values);
				}
				// Close the DroidGRRProvider Cursor
				cursor.close();
			}
		}
		// Done with the cursor, close it
		smsCursor.close();
	}

}
