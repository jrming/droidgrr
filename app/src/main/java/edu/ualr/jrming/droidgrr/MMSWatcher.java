package edu.ualr.jrming.droidgrr;

/**
 * MMSWatcher.java
 * @author Jonathan Ming
 * Based on MMS.java, MMSIncomingWatcher.java, and MMSOutgoingWatcher.java,
 * all of which were authored by Justin Grover
 *
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * This file: Copyright 2016 Jonathan Ming
 * Basis files: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

/** This class retrieves sent and retrieved MMS messages. **/
public abstract class MMSWatcher {

	private static final String TAG = "MMSWatcher";
    
    /**
	 * Retrieve MMS messages, using the supported API if on API 19 or higher and resorting
	 * to the unsupported content:/mms/ provider for older versions
     */
    public static void queryMMSLog(Context context, long startMillis, long endMillis) {

		if (endMillis < 0) {
			// If a start time is provided, but no end time, then set the end time to right now.
			endMillis = System.currentTimeMillis();
		} else if (startMillis < 0) {
			// If an end time is provided, but no start time, then set the start time to 0,
			// which effectively gets all events until the end time
			startMillis = 0;
		}
		// If neither start nor end time is provided, then leave both invalid
		// to exclude the selection entirely and get ALL events, ever (see line 72)

		// So, apparently the MMS content provider uses epoch time in seconds, not milliseconds.
		// This is unlike the SMS content provider which uses milliseconds.
		// ...I really, really wish this API was actually documented. That'd be great.
		long startSecs = startMillis / 1000;
		long endSecs = endMillis / 1000;

		if (Build.VERSION.SDK_INT >= 19) {
			supportedMmsQuery(context, startSecs, endSecs);
		} else {
			unsupportedMmsQuery(context, startSecs, endSecs);
		}
    }


	@TargetApi(19)
	private static void supportedMmsQuery(Context context, long startSecs, long endSecs) {
		Log.d(TAG, "Hit supportedMmsQuery");

		//----- First, get incoming MMS messages -----//
		// Initialize API-supported MMS content provider
		Uri mmsUri = Telephony.Mms.Inbox.CONTENT_URI;

		// Query for message id, address, date received, type, and body
		String[] mmsProj = {
				Telephony.Mms._ID,
				Telephony.Mms.MESSAGE_ID,
				Telephony.Mms.THREAD_ID,
				Telephony.Mms.DATE,
				Telephony.Mms.MESSAGE_BOX
		};
		String mmsSel = null;
		String[] mmsSelArgs = null;
		if (startSecs > 0 && endSecs > 0) {
			mmsSel = Telephony.Mms.DATE + " BETWEEN ? AND ?";
			mmsSelArgs = new String[]{ Long.toString(startSecs), Long.toString(endSecs) };
		}
		String mmsOrder = Telephony.Mms.DATE + " DESC";

		Cursor mmsCursor;
		try {
			mmsCursor = context.getContentResolver().query(mmsUri, mmsProj, mmsSel, mmsSelArgs, mmsOrder);
		} catch (Exception e) {
			Log.e(TAG, "Unable to query supported MMS Content Provider (Inbox)");
			return;
		}
		if (mmsCursor == null) {
			Log.e(TAG, "Supported MMS Content Provider (Inbox) returned null cursor");
			return;
		}

		readCursor(context, mmsCursor);

		//----- Next, get outgoing MMS messages -----//
		// Initialize API-supported MMS content provider
		mmsUri = Telephony.Mms.Sent.CONTENT_URI;

		try {
			mmsCursor = context.getContentResolver().query(mmsUri, mmsProj, mmsSel, mmsSelArgs, mmsOrder);
		} catch (Exception e) {
			Log.e(TAG, "Unable to query supported MMS Content Provider (Sent)");
			return;
		}
		if (mmsCursor == null) {
			Log.e(TAG, "Supported MMS Content Provider (Sent) returned null cursor");
			return;
		}

		readCursor(context, mmsCursor);
	}

	@TargetApi(14)
	private static void unsupportedMmsQuery(Context context, long startSecs, long endSecs) {
		Log.d(TAG, "Hit unsupportedMmsQuery");

		//----- First, get incoming MMS -----//
		// Initialize MMS content provider URI (undocumented)
		Uri mmsUri = Uri.parse("content://mms/inbox");

		// Query for message id, address, sender (person), date sent & received, type, and body
		String[] mmsProj = {
				"_id",
				"m_id",
				"thread_id",
				"date",
				"msg_box"
		};
		String mmsSel = null;
		String[] mmsSelArgs = null;
		if (startSecs > 0 && endSecs > 0) {
			mmsSel = "date BETWEEN ? AND ?";
			mmsSelArgs = new String[]{ Long.toString(startSecs), Long.toString(endSecs) };
		}
		String mmsOrder = "date DESC";
		Cursor mmsCursor;
		try {
			mmsCursor = context.getContentResolver().query(mmsUri, mmsProj, mmsSel, mmsSelArgs, mmsOrder);
		} catch(Exception e) {
			Log.e(TAG, "Unable to query URI: "+mmsUri.toString());
			return;
		}
		if (mmsCursor == null) {
			Log.e(TAG, "Unsupported MMS content provider (Inbox) returned null cursor");
			return;
		}

		readCursor(context, mmsCursor);

		//----- Next, get outgoing MMS -----//
		mmsUri = Uri.parse("content://mms/sent");

		try {
			mmsCursor = context.getContentResolver().query(mmsUri, mmsProj, mmsSel, mmsSelArgs, mmsOrder);
		} catch (Exception e) {
			Log.e(TAG, "Unable to query URI: "+mmsUri.toString());
			return;
		}
		if (mmsCursor == null) {
			Log.e(TAG, "Unsupported MMS content provider (Sent) returned null cursor");
			return;
		}

		readCursor(context, mmsCursor);
	}


	private static void readCursor(Context context, Cursor mmsCursor) {
		if (mmsCursor.getCount() > 0) {
			long id;
			String msgID;
			long threadID;
			Date date;
			String msgBox;


			Uri partUri = Uri.parse("content://mms/part");
			String[] partProj;
			String partSel;
			if (Build.VERSION.SDK_INT >= 19) {
				partProj = new String[]{
						Telephony.Mms.Part.CONTENT_TYPE,
						Telephony.Mms.Part.TEXT,
						Telephony.Mms.Part._DATA
				};
				partSel = Telephony.Mms.Part.MSG_ID + " = ?";
			} else {
				partProj = new String[]{
						"ct",
						"text",
						"_data"
				};
				partSel = "mid = ?";
			}
			String[] partSelArgs;	//Will be defined per each message
			Cursor partCursor;

			String ct;	//This variable will be used for looping and nothing more
			String contentType;	//This one will actually contain the media content type
			String msgPart;
			String msgText;


			Uri addrUri;
			String[] addrProj;
			String addrSel;
			if (Build.VERSION.SDK_INT >= 19) {
				addrProj = new String[]{
						Telephony.Mms.Addr.ADDRESS,
						Telephony.Mms.Addr.TYPE
				};
				addrSel = Telephony.Mms.Addr.MSG_ID + " = ?";
			} else {
				addrProj = new String[]{
						"address",
						"type"
				};
				addrSel = "msg_id = ?";
			}
			String[] addrSelArgs;
			Cursor addrCursor;

			ArrayList<String> addresses;
			String addressesString;
			String addrType;	//Used for looping only

			// Iterate over all MMS messages
			while (mmsCursor.moveToNext()) {
				id			= mmsCursor.getLong( mmsCursor.getColumnIndexOrThrow("_id") );
				msgID		= mmsCursor.getString( mmsCursor.getColumnIndexOrThrow("m_id") );
				threadID	= mmsCursor.getLong( mmsCursor.getColumnIndexOrThrow("thread_id") );
				date = new Date(mmsCursor.getLong( mmsCursor.getColumnIndexOrThrow("date") ));

				switch (mmsCursor.getInt( mmsCursor.getColumnIndexOrThrow("msg_box") )) {
					case 1:
						msgBox = "Received";
						break;
					case 2:
					case 4:
						msgBox = "Sent";
						break;
					case 5:
						msgBox = "Failed to send";
						break;
					default:
						// We don't want any type besides the ones above - skip to the next message
						continue;
				}


				// Query the MMS part Content Provider for more details about this message
				partSelArgs = new String[]{ Long.toString(id) };
				try {
					partCursor = context.getContentResolver().query(partUri, partProj, partSel, partSelArgs, null);
				} catch (Exception e) {
					Log.e(TAG, "Unable to query MMS Part Content Provider");
					return;
				}
				if (partCursor == null) {
					Log.e(TAG, "MMS Part Content Provider returned null cursor");
					return;
				}

				// Parse data from the Part cursor
				contentType = null;
				msgPart = null;
				msgText = null;
				while (partCursor.moveToNext()) {
					ct = partCursor.getString( partCursor.getColumnIndexOrThrow("ct") );
					switch (ct) {
						case "application/smil":
							continue;	//Ignore the SMIL file - skip to the next while() iteration
						case "text/plain":
							msgText = partCursor.getString( partCursor.getColumnIndexOrThrow("text") );
							break;
						default:
							contentType = ct;
							msgPart = partCursor.getString( partCursor.getColumnIndexOrThrow("_data") );
					}
					if (contentType != null && msgPart != null && msgText != null) {
						break;
					}
				}
				partCursor.close();


				// Query the MMS Address content provider to find the addresses for this message
				addrUri = Uri.parse("content://mms/"+id+"/addr");
				addrSelArgs = new String[]{ Long.toString(id) };
				try {
					addrCursor = context.getContentResolver().query(addrUri, addrProj, addrSel, addrSelArgs, null);
				} catch (Exception e) {
					Log.e(TAG, "Unable to query MMS Address Content Provider");
					return;
				}
				if (addrCursor == null) {
					Log.e(TAG, "MMS Address Content Provider returned null cursor");
					return;
				}

				//Parse data from the address cursor, populating the addresses ArrayList
				addresses = new ArrayList<>();
				while (addrCursor.moveToNext()) {
					// Google apparently was too lazy to document this, so I found it in the source
					// code. PduHeader type values are in com.google.android.mms.pdu.PduHeaders
					switch (addrCursor.getInt( addrCursor.getColumnIndexOrThrow("type") )) {
						case 0x97:	//TO
							addrType = "To: ";
							break;
						case 0x89:	//FROM
							addrType = "From: ";
							break;
						case 0x82:	//CC
							addrType = "CC: ";
							break;
						case 0x81:	//BCC
							addrType = "BCC: ";
							break;
						default:	//Either invalid or something we're not interested in
							continue;
					}
					addresses.add(
						addrType + addrCursor.getString( addrCursor.getColumnIndexOrThrow("address") )
					);
				}
				addrCursor.close();

				// Implode the addresses ArrayList into a nicely formatted String
				addressesString = "";
				for (String addr : addresses) {
					addressesString += addr + "; ";
				}
				addressesString = addressesString.substring(0, addressesString.length()-2);


				// Check the DroidWatch content provider to make sure message is not a duplicate
				Uri mmsUri = DroidGRRProvider.Mms.CONTENT_URI;
				String[] projection = { DroidGRRProvider.Mms.MSG_ID };
				String selection = DroidGRRProvider.Mms.MSG_ID + " = ?";
				String[] selectionArgs = { msgID };
				Cursor cursor;
				try {
					cursor = context.getContentResolver().query(mmsUri, projection, selection, selectionArgs, null);
				} catch (Exception e) {
					Log.e(TAG, "Unable to query DroidGRR MMS table");
					return;
				}
				if (cursor == null) {
					Log.e(TAG, "DroidGRR MMS Content Provider returned null cursor");
					return;
				}

				if (cursor.getCount() == 0) {
					// Insert new MMS into DroidGRR
					ContentValues values = new ContentValues();
					values.put(DroidGRRProvider.Mms.MSG_ID, msgID);
					values.put(DroidGRRProvider.Mms.THREAD_ID, threadID);
					values.put(DroidGRRProvider.Mms.ADDRESSES, addressesString);
					values.put(DroidGRRProvider.Mms.DATE, date.getTime());
					values.put(DroidGRRProvider.Mms.BOX, msgBox);
					values.put(DroidGRRProvider.Mms.MSG_CONTENT_TYPE, contentType);
					values.put(DroidGRRProvider.Mms.TEXT, msgText);
					values.put(DroidGRRProvider.Mms.DATA, msgPart);
					context.getContentResolver().insert(mmsUri, values);
				}
				// Close the DroidGRRProvider Cursor
				cursor.close();

			}
		}
		mmsCursor.close();
	}

}