package edu.ualr.jrming.droidgrr;

/**
 * PhotoWatcher.java
 * @author Jonathan Ming
 * Modified version of PhotoWatcher.java, authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import android.util.Log;

import java.util.Date;

/** This class detects newly added photos to the Android Gallery. **/
public abstract class PhotoWatcher {

	private static final String TAG = "PhotoWatcher";
	
	/** Retrieves image media from Android's Content Provider and stores it into the database */
	public static void queryPhotos(Context context, long startMillis, long endMillis) {

		if (endMillis < 0) {
			// If a start time is provided, but no end time, then set the end time to right now.
			endMillis = System.currentTimeMillis();
		} else if (startMillis < 0) {
			// If an end time is provided, but no start time, then set the start time to 0,
			// which effectively gets all events until the end time
			startMillis = 0;
		}

		// Query the MediaStore Content Provider for data about photos taken
		Uri photoUri = Media.EXTERNAL_CONTENT_URI;

		String[] photoProj = {
				Media._ID,
				Media.DATA,
				Media.DATE_TAKEN,
				Media.LATITUDE,
				Media.LONGITUDE
		};
		String photoSel = null;
		String[] photoSelArgs = null;
		// If start or end time is invalid, then omit the selection clause entirely
		if(startMillis > 0 && endMillis > 0) {
			photoSel = Media.DATE_TAKEN + " BETWEEN ? AND ?";
			photoSelArgs = new String[]{ Long.toString(startMillis), Long.toString(endMillis) };
		}
		String photoOrder = Media.DATE_TAKEN + " DESC";

		Cursor photoCursor;
		try {
			photoCursor = context.getContentResolver().query(photoUri, photoProj, photoSel, photoSelArgs, photoOrder);
		} catch(Exception e) {
			Log.e(TAG, "Unable to query MediaStore.Images.Media Content Provider");
			return;
		}
		if (photoCursor == null) {
			Log.e(TAG, "MediaStore.Images.Media Content Provider returned null cursor");
			return;
		}

		// Parse the data and store it into the database in preparation for transfer
		if (photoCursor.getCount() > 0) {
			long id;
			String filePath;
			Date date;
			double latCoord;
			double longCoord;

			while(photoCursor.moveToNext()) {
				// Get data from the photo cursor
				id = photoCursor.getLong(photoCursor.getColumnIndexOrThrow(Media._ID));
				filePath = photoCursor.getString(photoCursor.getColumnIndexOrThrow(Media.DATA));
				date = new Date(photoCursor.getLong(photoCursor.getColumnIndexOrThrow(Media.DATE_TAKEN)));
				latCoord = photoCursor.getDouble(photoCursor.getColumnIndexOrThrow(Media.LATITUDE));
				longCoord = photoCursor.getDouble(photoCursor.getColumnIndexOrThrow(Media.LONGITUDE));

				// Initialize DroidGRR events table URI
				Uri dbPhotoUri = DroidGRRProvider.Photos.CONTENT_URI;

				// Check to see if the photo is already in the local database
				String[] projection = { DroidGRRProvider.Photos.PHOTO_ID };
				String selection = DroidGRRProvider.Photos.PHOTO_ID + " = ?";
				String[] selectionArgs = { Long.toString(id) };

				Cursor cursor;
				try {
					cursor = context.getContentResolver().query(dbPhotoUri, projection, selection, selectionArgs, null);
				} catch(Exception e) {
					Log.e(TAG, "Unable to query photos table");
					return;
				}
				if (cursor == null) {
					Log.e(TAG, "DroidGRRProvider returned null cursor");
					return;
				}

				if (cursor.getCount() == 0) {
					// Insert newly added picture to the database
					ContentValues values = new ContentValues();
					values.put(DroidGRRProvider.Photos.PHOTO_ID, id);
					values.put(DroidGRRProvider.Photos.FILEPATH, filePath);
					values.put(DroidGRRProvider.Photos.DATE, date.getTime());
					values.put(DroidGRRProvider.Photos.LATITUDE, latCoord);
					values.put(DroidGRRProvider.Photos.LONGITUDE, longCoord);
					context.getContentResolver().insert(dbPhotoUri, values);
				} else {
					Log.w(TAG, "Photo ID match found in database: id=" + id + " | Cursor count=" + cursor.getCount());
				}
				// Close the DroidGRRProvider Cursor, next iteration gets a new query anyway
				cursor.close();
			}
		} else {
			Log.w(TAG, "Photo content provider returned empty cursor. No photos?");
		}
		// Done with the cursor, close it
		photoCursor.close();
	}
}