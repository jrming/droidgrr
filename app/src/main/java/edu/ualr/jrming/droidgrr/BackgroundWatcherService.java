package edu.ualr.jrming.droidgrr;

/**
 * BackgroundWatcherService.java
 * @author Jonathan Ming
 * Modified version of WatcherService.java, authored by Justin Grover
 *
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.util.Log;

/** This class performs the startup and cancellation of the DroidGRR components. **/
public class BackgroundWatcherService extends Service {
	private static final String TAG = "BGWatcherService";

	// This will hold the Thread this service runs in, so it can be properly destroyed on stop
	private Handler mWatcherHandler;

	// Initialize BroadcastReceivers
	private BroadcastReceiver 		screenStatusReceiver;		// Detects screen locks,unlocks		+
	private BroadcastReceiver 		appInstallRemoveReceiver;	// Detects app installs,removals	+


	/** BackgroundWatcherService doesn't permit binding, so return null. */
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	/** Creates the Service and fires up the Watchers. Called by the system after startService() */
	@Override
	public void onCreate() {
		super.onCreate();

		HandlerThread thread = new HandlerThread("WatcherWorker",
													Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// Get the HandlerThread's Looper and use it for our Handler
		Looper workerLooper = thread.getLooper();
		mWatcherHandler = new Handler(workerLooper);
		mWatcherHandler.post(new Runnable() {
			@Override
			public void run() {
				Log.v(TAG,"WatcherWorker: Starting background watchers...");
				startWatchers();
				Log.v(TAG,"WatcherWorker: Finished starting background watchers.");
			}
		});
	}

	/** Starts the Service and sets it to be persistent. Called by the system after onCreate() */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		return Service.START_STICKY;
	}


	/** Takes care of starting the various Watchers */
	private void startWatchers() {
		//Get app installs and removals
		appInstallRemoveReceiver = new AppWatcher();
		IntentFilter appInstallRemoveFilter = new IntentFilter();
		appInstallRemoveFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
		appInstallRemoveFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		appInstallRemoveFilter.addDataScheme("package");
		registerReceiver(appInstallRemoveReceiver, appInstallRemoveFilter);

		//Get screen status updates
		screenStatusReceiver = new ScreenWatcher();
		IntentFilter screenStatusFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		screenStatusFilter.addAction(Intent.ACTION_SCREEN_OFF);
		screenStatusFilter.addAction(Intent.ACTION_USER_PRESENT);
		registerReceiver(screenStatusReceiver, screenStatusFilter);
	}

	/** Unregisters the BroadcastReceiver Watchers */
	private void stopWatchers() {
		if (appInstallRemoveReceiver != null) {
			unregisterReceiver(appInstallRemoveReceiver);
		}
		if (screenStatusReceiver != null) {
			unregisterReceiver(screenStatusReceiver);
		}
	}


	/** Called by the system before the Service is destroyed. Handles destroying active Watchers. */
	@Override
	public void onDestroy() {
		// Cancel Service Components
		mWatcherHandler.removeCallbacksAndMessages(null);

		Log.v(TAG,"WatcherWorker: Stopping background watchers...");
		stopWatchers();
		Log.v(TAG,"WatcherWorker: Finished stopping background watchers.");

		super.onDestroy();
	}


}