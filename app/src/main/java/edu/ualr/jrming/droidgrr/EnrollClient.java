package edu.ualr.jrming.droidgrr;

/**
 * EnrollClient.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.Preference;
import android.util.Log;

import com.google.protobuf.ByteString;

import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x500.X500NameBuilder;
import org.spongycastle.asn1.x509.X509ObjectIdentifiers;
import org.spongycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.spongycastle.openssl.jcajce.JcaPEMWriter;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.pkcs.PKCS10CertificationRequest;
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.spongycastle.util.io.pem.PemObject;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Properties;

/** AsyncTask responsible for enrolling this DroidGRR client with the GRR server */
public class EnrollClient extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = "EnrollClient";

    // The context used to launch this action. Used only to display a Toast at finish.
    private Context mContext;
    // The Preference selected to launch this action. Used only by publishProgress.
    private Preference mSourcePreference;
    // SharedPreference objects to access global metadata about this client (IDs and such)
    private SharedPreferences mClientPrefs;


    public EnrollClient(Context context) {
        mContext = context;
        mClientPrefs = context.getSharedPreferences(
            ClientContract.CLIENT_PREFS, Context.MODE_PRIVATE
        );
    }
    public EnrollClient(Context context, Preference pref) {
        this(context);
        mSourcePreference = pref;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            publishProgress("Obtaining common name");
            String commonName = mClientPrefs.getString(ClientContract.CLIENT_CN, null);
            if (commonName == null) {
                Log.i(TAG, "generateCSR() couldn't find the client name. Generating a new one.");
                publishProgress("Generating a Client ID");

                // Open the app properties to retrieve the RSA key length
                Properties props = PropertyManager.openProperties(mContext);
                if (props == null) {
                    Log.e(TAG, "Unable to obtain app properties. Aborting ID generation.");
                    return null;
                }
                int keyLen;
                try {
                    keyLen = Integer.parseInt(props.getProperty("rsa_key_length"));
                    if (keyLen < 1024 || (keyLen & (keyLen - 1)) != 0) {
                        // If keyLen is not a power of 2 greater than or equal to 1024, reject it
                        throw new NumberFormatException();
                    }
                } catch (NumberFormatException e) {
                    Log.w(TAG, "Bad RSA key length provided in droidgrr.properties; defaulting to 2048.");
                    keyLen = 2048;
                }

                // We've got the key length; now generate the client ID
                commonName = Communicator.generateClientId(mContext, keyLen);
            }

            publishProgress("Generating CSR");
            ByteString csrByteString = generateCSR(commonName);
            if (csrByteString == null || csrByteString.isEmpty()) {
                throw new Exception("Could not generate CSR.");
            }

            publishProgress("Constructing message");
            JobProtos.GrrMessage message = constructMessage(csrByteString);

            publishProgress("Sending message");
            Communicator.sendMessages(mContext, message);

        } catch (Exception e) {
            Log.e(TAG, "Client enrollment failed! " + e.getLocalizedMessage());
            return false;
        }
        return true;
    }


    /**
     * Generates a PEM-encoded X.509 Certificate Signing Request and returns it as a ByteString.
     * Takes care of locating requisite client data (RSA key pair, Client ID) or generating it
     * if it doesn't already exist.
     */
    private ByteString generateCSR(String commonNameString) {
        try {
            X500Name commonName = (new X500NameBuilder())
                    .addRDN(X509ObjectIdentifiers.commonName, commonNameString)
                    .build();

            // First, obtain the our public and private keys from the KeyStore
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);
            // Neither of the below should return null, since we already obtained the common name,
            // but if either do, then the inevitable NullPointerExceptions will get caught by this
            // method's outside try-catch and enrollment will stop.
            PrivateKey clientPrivateKey = (PrivateKey) ks.getKey(ClientContract.CLIENT_KEYS, null);
            PublicKey clientPublicKey = ks.getCertificate(ClientContract.CLIENT_KEYS).getPublicKey();

            JcaPKCS10CertificationRequestBuilder csrBuilder =
                    new JcaPKCS10CertificationRequestBuilder(
                            commonName,
                            clientPublicKey
                    );

            ContentSigner signer =
                    (new JcaContentSignerBuilder("SHA256WITHRSA")).build(clientPrivateKey);

            PKCS10CertificationRequest csr = csrBuilder.build(signer);

            PemObject csrPem = (new JcaMiscPEMGenerator(csr)).generate();

            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            JcaPEMWriter jpw = new JcaPEMWriter(new OutputStreamWriter(byteOutStream));

            jpw.writeObject(csrPem);
            jpw.close();
            byte[] csrBytes = byteOutStream.toByteArray();
            byteOutStream.close();

            return ByteString.copyFrom(csrBytes);

        } catch (Exception e) {
            Log.e(TAG, "CSR Generation Failed! " + e.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Constructs the CSR and necessary metadata into a ClientCommunication protobuf message.
     * @param csrByteString A ByteString containing the encoded CSR to send
     * @return A MessageList protocol buffer ready to be signed and encrypted
     */
    private JobProtos.GrrMessage constructMessage(ByteString csrByteString) {
        // OK! Ready for a bumpy ride? No? Too bad! The dev's of GRR decided they liked their
        // code a nice spaghetti mess of excessive polymorphism, wrappers, and serialization,
        // which means we have to jump through ALL their VERY NUMEROUS hoops for this to work!
        // The CSR must be inserted into this object structure for the server to parse it:
        // CSR (PEM format plaintext)
        //      Certificate (encode CSR to ByteString)
        //          GrrMessage (serialize Certificate to ByteString)
        // ---------- The part below is handled by Communicator.java ----------
        //              MessageList (append GrrMessage object to jobs list)
        //                  ClientCommunication (sign the MessageList into a SignedMessageList,
        //                                       serializing it and compressing, then serialize and
        //                                       encrypt the SignedMessageList. Finally done!)
        JobProtos.Certificate cert =
                JobProtos.Certificate.newBuilder()
                        .setType(JobProtos.Certificate.Type.CSR)
                        .setPem(csrByteString)
                        .build();

        return JobProtos.GrrMessage.newBuilder()
                .setSessionId("aff4:/flows/E:Enrol")
                .setArgs(cert.toByteString())
                .setArgsRdfName("Certificate")
                .setTaskId(Communicator.generateTaskID(mContext))
                .build();
    }


    @Override
    protected void onProgressUpdate(String... values) {
        if (mSourcePreference != null) {
            for (String s : values) {
                mSourcePreference.setSummary(s);
                Log.i(TAG, s);
            }
        } else {
            for (String s : values) {
                Log.i(TAG, s);
            }
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mSourcePreference != null) {
            mSourcePreference.setSummary("");
        }
    }

}
