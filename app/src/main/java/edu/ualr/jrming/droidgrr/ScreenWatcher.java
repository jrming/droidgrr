package edu.ualr.jrming.droidgrr;

/**
 * ScreenWatcher.java
 * @author Jonathan Ming
 * Modified version of ScreenWatcher.java, authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/** This class detects changes to the screen's lock status (e.g., locked, unlocked, off). **/
public class ScreenWatcher extends BroadcastReceiver {
	// Initialize constants and variables
	private static final String TAG = "ScreenWatcher";
	
	/**
	 * This method handles a broadcasted intent.
	 * 
	 * @param context	The application context.
	 * @param intent	The broadcasted intent.
	 */
	@Override
	public void onReceive(final Context context, Intent intent)  {
		// Get screen status
		String action = intent.getAction();
		
		// Log event
		Uri eventsUri = DroidGRRProvider.Events.CONTENT_URI;
		ContentValues values = new ContentValues();
		switch (action) {
			case Intent.ACTION_SCREEN_OFF:
				values.put(DroidGRRDatabase.DETECTOR_COLUMN, TAG);
				values.put(DroidGRRDatabase.EVENT_ACTION_COLUMN, "Screen Off");
				values.put(DroidGRRDatabase.EVENT_DATE_COLUMN, System.currentTimeMillis());
				break;
			case Intent.ACTION_SCREEN_ON:
				values.put(DroidGRRDatabase.DETECTOR_COLUMN, TAG);
				values.put(DroidGRRDatabase.EVENT_ACTION_COLUMN, "Screen Locked");
				values.put(DroidGRRDatabase.EVENT_DATE_COLUMN, System.currentTimeMillis());
				break;
			case Intent.ACTION_USER_PRESENT:
				values.put(DroidGRRDatabase.DETECTOR_COLUMN, TAG);
				values.put(DroidGRRDatabase.EVENT_ACTION_COLUMN, "Screen Unlocked");
				values.put(DroidGRRDatabase.EVENT_DATE_COLUMN, System.currentTimeMillis());
				break;
			default:
				return;
		}
		context.getContentResolver().insert(eventsUri, values);
	}
}