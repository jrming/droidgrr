package edu.ualr.jrming.droidgrr;

/**
 * ContactWatcher.java
 * @author Jonathan Ming
 * Modified version of ContactWatcher.java, authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;

/** This interface contains a method to retrieve the contacts list. **/
public abstract class ContactWatcher {
	// Initialize constants and variables
	private static final String TAG = "ContactWatcher";

	/**
	 * This method looks for new contacts in the contacts content provider and compares
	 * them to what exists already in the DroidWatch contacts table.
	 */
	public static void queryContacts(Context context) {
		// Query the contacts Content Provider
		String[] contactsProj = { Phone._ID, Phone.DISPLAY_NAME, Phone.NUMBER };
		Cursor contactsCursor;
		try {
			contactsCursor = context.getContentResolver().query(Phone.CONTENT_URI, contactsProj, null, null, null);
		}
		catch(Exception e) {
			Log.e(TAG, "Unable to query contacts content provider");
			return;
		}
		
		// Parse the query results
		if (contactsCursor == null) {
			Log.e(TAG, "Unable to query contacts content provider");
			return;
		}
		
		if (contactsCursor.getCount() > 0) {
			long id;
			String name;
			String phoneNumber;
			long currentTime = System.currentTimeMillis();

			while (contactsCursor.moveToNext()) {
				id = contactsCursor.getLong(contactsCursor.getColumnIndexOrThrow(Phone._ID));
				name = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Phone.DISPLAY_NAME));
				phoneNumber = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Phone.NUMBER));

				// Check to see if this contact already exists in the DroidWatch contacts table
				String[] projection = { DroidGRRProvider.Contacts.ID };
				String selection = DroidGRRProvider.Contacts.ID + " = ?";
				String[] selectionArgs = { Long.toString(id) };
				Cursor cursor;
				try {
					cursor = context.getContentResolver().query(
							DroidGRRProvider.Contacts.CONTENT_URI,
							projection,
							selection,
							selectionArgs, null);
				}
				catch(Exception e) {
					Log.e(TAG, "Error querying contacts table in results.db: "+e.getMessage());
					return;
				}
				if (cursor == null) {
					Log.e(TAG, "Error querying contacts table in results.db");
					return;
				}

				if (cursor.getCount() == 0) {
					// Insert new contact into the DroidWatch contacts table if it isn't already there
					ContentValues contactValues = new ContentValues();
					contactValues.put(DroidGRRDatabase.CONTACT_ID_COLUMN, id);
					contactValues.put(DroidGRRDatabase.CONTACT_NAME_COLUMN, name);
					contactValues.put(DroidGRRDatabase.CONTACT_ADDED_COLUMN, currentTime);
					contactValues.put(DroidGRRDatabase.CONTACT_NUMBER_COLUMN, phoneNumber);
					context.getContentResolver().insert(DroidGRRProvider.Contacts.CONTENT_URI, contactValues);
				}
				// Close the DroidGRRProvider Cursor
				cursor.close();
        	}
		}
		// Done with the cursor, close it
		contactsCursor.close();
	}
}