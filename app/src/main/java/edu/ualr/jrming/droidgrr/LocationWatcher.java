package edu.ualr.jrming.droidgrr;

/**
 * LocationWatcher.java
 * @author Jonathan Ming
 * Named after LocationWatcher.java, by Justin Grover, but the contents were completely rewritten
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/** The watcher responsible for retrieving the device's current fine location */
public class LocationWatcher
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LocationWatcher";

    private Context mContext;
    private GoogleApiClient mApiCli;

    /**
     * Constructs an instance of LocationWatcher, which does nothing but define the context in which
     * to request the location. To actually retrieve location, call queryLocation() on the
     * newly constructed object.
     * @param context The context in which to request the device's location
     */
    public LocationWatcher(Context context) {
        mContext = context;
    }

    public void queryLocation() {
        mApiCli = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mApiCli.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle connectionHint) {
        int perm = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
        if (perm != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "Permission denied to access location!");
            mApiCli.disconnect();
            return;
        }
        Location loc = LocationServices.FusedLocationApi.getLastLocation(mApiCli);

        if (loc == null) {
            Log.e(TAG, "Location services returned null object! GPS may be unavailable.");
            mApiCli.disconnect();
            return;
        }

        // Insert the new location into DroidWatch
        ContentValues values = new ContentValues();
        values.put(DroidGRRDatabase.DETECTOR_COLUMN, TAG);
        values.put(DroidGRRDatabase.EVENT_ACTION_COLUMN, "LastKnownLocation Received");
        values.put(DroidGRRDatabase.EVENT_DATE_COLUMN, loc.getTime());
        values.put(DroidGRRDatabase.EVENT_DESCRIPTION_COLUMN, "Lat:"+loc.getLatitude()+"; Lng:"+loc.getLongitude());
        mContext.getContentResolver().insert(DroidGRRProvider.Events.CONTENT_URI, values);

        mApiCli.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Google API connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection to Google API failed! Unable to retrieve device location.");
    }
}