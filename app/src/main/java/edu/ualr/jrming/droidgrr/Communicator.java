package edu.ualr.jrming.droidgrr;

/**
 * Communicator.java
 * @author Jonathan Ming
 *
 * NOTICE - This software is intended to serve as prototype material.
 *
 * Copyright 2016 Jonathan Ming
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.widget.Toast;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import org.spongycastle.util.Arrays;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.UnsupportedAddressTypeException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPublicKey;
import java.util.Properties;
import java.util.Random;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * An {@link IntentService} for communicating with the GRR server. Handles actions that must
 * be taken for every message, such as MessageList and ClientCommunication construction.
 */
public class Communicator extends IntentService {
    private static final String TAG = "Communicator";

    private static final String ACTION_SEND_MESSAGE = "edu.ualr.jrming.droidgrr.action.SEND_MESSAGE";
    private static final String ACTION_POLL_ONCE = "edu.ualr.jrming.droidgrr.action.POLL_ONCE";

    private static final String EXTRA_MESSAGE_LIST = "edu.ualr.jrming.droidgrr.extra.MESSAGE_LIST";

    private static long mNonce;


    public Communicator() {
        super("Communicator");
    }


    /** Sends the given list of GrrMessages to the server */
    public static void sendMessages(Context context, JobProtos.GrrMessage... messages) {
        JobProtos.MessageList.Builder messageListBuilder = JobProtos.MessageList.newBuilder();
        for (JobProtos.GrrMessage msg : messages) {
            messageListBuilder.addJob(msg);
        }
        byte[] serializedMessageList = messageListBuilder.build().toByteArray();

        Intent intent = new Intent(context, Communicator.class);
        intent.setAction(ACTION_SEND_MESSAGE);
        intent.putExtra(EXTRA_MESSAGE_LIST, serializedMessageList);
        context.startService(intent);
    }

    /** Polls the server once to see if there are waiting messages */
    public static void pollOnce(Context context) {
        Intent intent = new Intent(context, Communicator.class);
        intent.setAction(ACTION_POLL_ONCE);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            // Verify that we're connected to the Internet
            ConnectivityManager connMgr =
                    (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                // We're connected - go ahead with whatever action was requested
                final String action = intent.getAction();
                switch (action) {
                    case ACTION_SEND_MESSAGE:
                        handleSendMessage( intent.getByteArrayExtra(EXTRA_MESSAGE_LIST) );
                        break;
                    case ACTION_POLL_ONCE:
                        JobProtos.MessageList messageList = JobProtos.MessageList.getDefaultInstance();
                        handleSendMessage(messageList.toByteArray());
                        break;
                }
            } else {
                // We're not connected - warn the user and suppress the requested action
                Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        }
    }


    /** In the background thread, send the given (serialized) MessageList to the server */
    private void handleSendMessage(byte[] messageList) {
        // Retrieve needed data from app properties
        Properties props = PropertyManager.openProperties(this);
        if (props == null) {
            Log.e(TAG, "Unable to obtain app properties. Aborting message send.");
            return;
        }
        URL serverUrl = null;
        int keyLen;
        try {
            serverUrl = new URL(props.getProperty("server_url"));
            keyLen = Integer.parseInt( props.getProperty("rsa_key_length") );
            if ( keyLen < 1024 || (keyLen & (keyLen - 1)) != 0 ) {
                // If keyLen is not a power of 2 greater than or equal to 1024, reject it
                throw new NumberFormatException();
            }
        } catch (MalformedURLException urlE) {
            Log.e(TAG, "Malformed URL provided in droidgrr.properties. Aborting send.");
            return;
        } catch (NumberFormatException numE) {
            Log.w(TAG, "Bad RSA key length provided in droidgrr.properties; defaulting to 2048.");
            keyLen = 2048;
        }


        // First, check to see if this client has a common name (ID). If it doesn't, generate one.
        SharedPreferences clientPrefs = getSharedPreferences(
                ClientContract.CLIENT_PREFS, Context.MODE_PRIVATE
        );
        String commonNameString = clientPrefs.getString(ClientContract.CLIENT_CN, null);
        if (commonNameString == null) {
            // We don't have a client ID / common name, so we need to generate a new one.
            try {
                commonNameString = generateClientId(keyLen);
            } catch (Exception e) {
                Log.e(TAG, "Failed to generate a new client KeyPair and ID: "
                        + e.getLocalizedMessage()
                );
                return;
            }
        }


        // Next, verify that we can connect to the server and that it has a valid certificate
        try {
            verifyServer(serverUrl);
        } catch (Exception e) {
            Log.e(TAG, "Unable to verify server: " + e.getLocalizedMessage());
            return;
        }
        // If we make it to this point, then the server is verified and it's public key should be
        // stored in our KeyStore for later. Next, we construct the Protobuf message structure.

        // Builder for the final ClientCommunication object we'll send
        JobProtos.ClientCommunication.Builder clientCommBuilder =
                JobProtos.ClientCommunication.newBuilder()
                        .setApiVersion(3);

        // We need to compress the MessageList into a SignedMessageList first:
        JobProtos.SignedMessageList signedMessageList = compressMessageList(messageList);
        // GRR uses the nanosecond timestamp as a nonce to protect against replay attacks.
        // We'll keep this nonce for now and use it to verify the server's response to our request.
        mNonce = signedMessageList.getTimestamp();

        // Next, we need to construct a CipherUtils object for encrypting the SignedMessageList.
        // CipherUtils requires this client's private key and the server's public one to operate,
        // so open the KeyStore to retrieve the necessary keys.
        PrivateKey clientPrivateKey;
        PublicKey serverPublicKey;
        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);
            clientPrivateKey = (PrivateKey) ks.getKey(ClientContract.CLIENT_KEYS, null);
            serverPublicKey = ks.getCertificate(ClientContract.SERVER_CERT).getPublicKey();
        } catch (Exception e) {
            Log.e(TAG, "Error opening the Android Keystore, aborting: " + e.getLocalizedMessage());
            return;
        }
        // Now that we've gotten the keys, pass them into CipherUtils, which will use them to
        // generate the metadata we need to encrypt and send the message
        CipherUtils cipherUtils = new CipherUtils(
                commonNameString,
                clientPrivateKey,
                serverPublicKey
        );
        // TODO: Investigate the way GRR temporarily caches its Cipher objects and see if porting
        // that somehow is a good idea. For now, just don't cache at all.

        // Get the cipher properties and metadata
        byte[] encryptedCipher = cipherUtils.getEncryptedCipher();
        byte[] encryptedCipherMetadata = cipherUtils.getEncryptedCipherMetadata();
        // Generate a new encryption IV that we'll use to encrypt the SignedMessageList
        byte[] packetIv = cipherUtils.createNewIv();
        // Encrypt the SignedMessageList
        byte[] encrypted = cipherUtils.encrypt(signedMessageList, packetIv);

        // Create a GRR Full Hash of the message:
        // HMAC of the 4 previous fields plus the API version stored as a little-endian 32-bit int
        // First, concatenate the bytes of the 4 fields, in the order the server is expecting
        byte[] hashBytesMissingApi = Arrays.concatenate(
                encrypted,
                encryptedCipher,
                encryptedCipherMetadata,
                packetIv
        );
        // Next, store the API version as a little-endian sequence of 4 bytes
        // Note that we don't have to flip() because we only want the array, and the buffer's
        // allocated size is exactly the same size as the size of the input data.
        byte[] apiBytes = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
                .putInt( clientCommBuilder.getApiVersion() )
                .array();
        // Finally, concatenate the arrays to the final sequence of bytes to be hashed and
        // calculate the HMAC of those bytes.
        byte[] fullHmac = cipherUtils.hmac( Arrays.concatenate(hashBytesMissingApi, apiBytes) );


        // Now that we've collected all the necessary data, build the ClientCommunication protobuf
        JobProtos.ClientCommunication clientComm = clientCommBuilder
            .setEncryptedCipher(            ByteString.copyFrom( encryptedCipher ))
            .setEncryptedCipherMetadata(    ByteString.copyFrom( encryptedCipherMetadata ))
            .setPacketIv(                   ByteString.copyFrom( packetIv ))
            .setEncrypted(                  ByteString.copyFrom( encrypted ))
            .setFullHmac(                   ByteString.copyFrom( fullHmac ))
        .build();


        HttpURLConnection conn;
        URL controlUrl;
        try {
            controlUrl = new URL(serverUrl, "control?api=3");
            conn = (HttpURLConnection) controlUrl.openConnection();
        } catch (IOException e) {
            Log.e(TAG, "Failed to open connection: " + e.getLocalizedMessage());
            return;
        }
        try {
            // Set 10-second timeouts for connect and read (specified in milliseconds)
//            conn.setConnectTimeout(10000);
//            conn.setReadTimeout(10000);

            conn.setDoOutput(true);
            conn.setRequestProperty("Accept-Encoding", "identity");
            conn.setRequestProperty("Content-Type", "application/octet-stream");
            conn.setFixedLengthStreamingMode(clientComm.getSerializedSize());

            OutputStream request = conn.getOutputStream();
            clientComm.writeTo(request);
            request.close();

            Log.i(TAG, "Sent messages to the server: " + conn.getResponseCode() + " " + conn.getResponseMessage());

            // Read the server's response. We're hoping for a 200 response code and an accompanying
            // payload containing messages for the client to process.
            BufferedInputStream response = new BufferedInputStream( conn.getInputStream() );

            // Attempt to verify and decrypt the server's response.
            // See grr/client/comms.py > GRRHTTPClient.VerifyServerControlResponse()
            if (conn.getResponseCode() == 200) {
                JobProtos.ClientCommunication responseComm;
                try {
                     responseComm = JobProtos.ClientCommunication.parseFrom(response);
                } catch (IOException e) {
                    throw new Exception("Unable to decode server response.");
                }

                JobProtos.MessageList responseMessages =
                        decodeMessages(responseComm, clientPrivateKey, serverPublicKey, mNonce);

                Log.i(TAG, "Received " + responseMessages.getJobCount() + " messages from the server.");

            } else {
                Log.e(TAG, "Aborting - server responded with " + conn.getResponseCode() + " " + conn.getResponseMessage());
                return;
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to complete transaction: (" + e.getClass() + ") " + e.getLocalizedMessage());
            return;
        } finally {
            conn.disconnect();
        }

        Log.d(TAG, "Transaction complete.");
    }


    /**
     * Verifies that the given URL points to an online and valid GRR server by connecting to the
     * given URL and asking for it's certificate, which is then verified against the app's CA cert.
     * Throws one of the below exceptions if verification fails; otherwise, the server is considered
     * verified and its public key is added to the app's KeyStore for reuse.
     *
     * @param serverUrl The URL of the server to verify
     * @throws IOException If an error occurs opening the connection, reading its stream, or
     * obtaining the CA certificate from this app's assets.
     * @throws GeneralSecurityException If an error occurs parsing the server's certificate or our
     * CA certificate, or if the server's public key fails to validate against our CA certificate.
     */
    private void verifyServer(URL serverUrl) throws IOException, GeneralSecurityException {
        // Holder for the server certificate we're going to retrieve
        Certificate serverCertificate;

        URL certUrl = new URL(serverUrl, "server.pem");
        HttpURLConnection certConn = (HttpURLConnection) certUrl.openConnection();
        try {
            // Set 10-second timeouts for connect and read (specified in milliseconds)
            certConn.setConnectTimeout(10000);
            certConn.setReadTimeout(10000);

            // Open an input stream to read the server's response
            BufferedInputStream certInStream = new BufferedInputStream(certConn.getInputStream());

            // Ensure the URL we've opened connection to is the one we want (no redirection)
            if (!certUrl.getHost().equals(certConn.getURL().getHost())) {
                // We were redirected! Abort via an exception.
                certInStream.close();
                throw new UnsupportedAddressTypeException();
            }

            // If we made it here, then we've got a good stream for the certificate.
            // Now we use a CertificateFactory to store it as a Certificate object.
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            serverCertificate = cf.generateCertificate(certInStream);

            certInStream.close();
        } finally {
            certConn.disconnect();
        }

        // Retrieve the CA certificate from our assets
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInputStream = getAssets().open("server_ca.crt");
        Certificate caCertificate = cf.generateCertificate(caInputStream);
        caInputStream.close();

        // Verify the recently retrieved server cert against the our CA certificate
        serverCertificate.verify(caCertificate.getPublicKey());

        // If we made it here, then the server certificate was verified; store it in our KeyStore
        KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
        ks.load(null);
        ks.setCertificateEntry(ClientContract.SERVER_CERT, serverCertificate);
        //TODO: Serial number verification (see grr/client/comms.py > ClientCommunicator.LoadServerCertificate)
    }


    /**
     * Generates a new KeyPair and Client ID which can be used as an X.509 Common Name.
     * Should only be run once, the first time this client tries to connect to the GRR server.
     * Stores the generated KeyPair to this app's KeyStore, and stores the common name to the
     * client SharedPreferences.
     * (See grr/lib/rdfvalues/client.py > ClientURN.FromPublicKey)
     * @param context The context with which to store the generated ID to SharedPreferences
     * @param keyLength The length, in bits, that the new RSA KeyPair should have
     * @return The newly generated Client ID (common name) as a String
     * @throws GeneralSecurityException If an error occurs generating a KeyPair or using it to hash.
     */
    public static String generateClientId(Context context, int keyLength) throws GeneralSecurityException {
        Log.i(TAG, "Generating RSA key pair");
        KeyPair clientKeys;

        // API level 23 improved key pair generation and security, so use the new API if available
        if (Build.VERSION.SDK_INT >= 23) {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore"
            );
            kpg.initialize(
                new KeyGenParameterSpec.Builder(
                        ClientContract.CLIENT_KEYS,
                        KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_DECRYPT)
                    .setDigests(KeyProperties.DIGEST_SHA1, KeyProperties.DIGEST_SHA256)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                    .setSignaturePaddings(
                            KeyProperties.SIGNATURE_PADDING_RSA_PSS,
                            KeyProperties.SIGNATURE_PADDING_RSA_PKCS1
                    )
                .build()
            );
            clientKeys = kpg.generateKeyPair(); // AndroidKeyStore automatically stores the key
        } else {
            // The new API isn't available, so do it the old (i.e. deprecated) way
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
            KeyPairGeneratorSpec.Builder kpgProperties = new KeyPairGeneratorSpec.Builder(context)
                .setAlias(ClientContract.CLIENT_KEYS);

            // It makes absolutely no sense to me why this would be left out of API version 18,
            // but it is, so I have to deal with the fallout. Excuse this mess; I wish everyone
            // would just use newer versions of Android, but I can't force them to.
            if (Build.VERSION.SDK_INT >= 19) {
                //noinspection WrongConstant
                kpgProperties.setKeyType("RSA").setKeySize(keyLength);
            }

            kpg.initialize(
                kpgProperties.build()
            );
            clientKeys = kpg.generateKeyPair(); // AndroidKeyStore automatically stores the key
        }

        // From this point on, we're attempting to emulate this function as exactly as possible:
        //      grr/lib/rdfvalues/client.py > ClientURN.FromPublicKey()
        // Our CN will be the first 64 bits of the hash of the public key
        // in MPI format - the length of the key in 4 bytes + the key
        // prefixed with a 0. This weird format is an artifact from the way
        // M2Crypto handled this, we have to live with it for now.

        // Get the public modulus from the key - this will be used to generate the client ID
        BigInteger publicModulus = ((RSAPublicKey) clientKeys.getPublic()).getModulus();
        byte[] modBytes = publicModulus.toByteArray();

        // Put the data parts (modulus length and modulus) into a ByteBuffer to prep for the hash
        ByteBuffer buffer = ByteBuffer.allocate( modBytes.length+4 );
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putInt(modBytes.length)
            .put(modBytes);

        // Flip the buffer to tell it we're done writing, time to read
        buffer.flip();

        // Get a MessageDigest instance for hashing with SHA-256
        MessageDigest hash;
        try {
            hash = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Log.wtf(TAG, "java.security.MessageDigest apparently no longer supports SHA-256.");
            throw new RuntimeException("Communicator:406 - MessageDigest Algorithm Not Found");
        }

        // Supply the MessageDigest with our ByteBuffer, then hash it into a byte array
        hash.update(buffer);
        byte[] digest = hash.digest();

        // Construct the final client ID using the first 8 bytes of the hash in hexadecimal
        String commonName = "C.";
        for (int i = 0; i < 8; i++) {
            commonName += String.format("%02x", digest[i]);
        }

        // Store the newly generated common name to the client's preferences to be reused
        SharedPreferences clientPrefs = context.getSharedPreferences(
            ClientContract.CLIENT_PREFS, Context.MODE_PRIVATE
        );
        clientPrefs.edit().putString(ClientContract.CLIENT_CN, commonName).apply();

        return commonName;
    }
    /**
     * A convenience method for calling {@link #generateClientId(Context, int)} using
     * {@literal this} for the Context value.
     */
    private String generateClientId(int keyLength) throws GeneralSecurityException {
        return generateClientId(this, keyLength);
    }

    /**
     * Compresses the given MessageList (pre-serialized into a byte array) into a SignedMessageList.
     * @param serializedMessageList The output of calling toByteArray() on the MessageList to send
     * @return A SignedMessageList containing a compressed version of the serialized MessageList
     */
    private static JobProtos.SignedMessageList compressMessageList(byte[] serializedMessageList) {
        JobProtos.SignedMessageList.Builder signedListBuilder =
                JobProtos.SignedMessageList.newBuilder()
                        .setTimestamp( System.currentTimeMillis() * 1000L ); // GRR wants nanos...

        // Compress the serialized MessageList and store it into the SignedMessageList
        try {
            // Just to be safe, make the compression buffer larger than it needs to be at first
            byte[] compressionBuffer = new byte[serializedMessageList.length+11];
            Deflater compressor = new Deflater();
            compressor.setInput(serializedMessageList);
            compressor.finish();
            int compressedSize = compressor.deflate(compressionBuffer);
            compressor.end();

            signedListBuilder.setMessageList(
                    // Run the buffer through copyOf to trim off the extra space
                    ByteString.copyFrom( java.util.Arrays.copyOf(compressionBuffer, compressedSize) )
            );
            signedListBuilder.setCompression(
                    JobProtos.SignedMessageList.CompressionType.ZCOMPRESSION
            );
        } catch (Exception e) {
            Log.w(TAG, "Failed to compress the MessageList. Sending it uncompressed.");
            signedListBuilder.setMessageList(
                    ByteString.copyFrom(serializedMessageList)
            );
            signedListBuilder.setCompression(
                    JobProtos.SignedMessageList.CompressionType.UNCOMPRESSED
            );
        }
        // The signed list is complete - build and return it.
        return signedListBuilder.build();
    }

    /**
     * Decompresses the given SignedMessageList into a readable MessageList.
     * @param signedMessageList The SignedMessageList to decompress
     * @return A MessageList representing the decompressed contents of the given SignedMessageList
     */
    private static JobProtos.MessageList decompressMessageList(JobProtos.SignedMessageList signedMessageList) {
        byte[] data;

        if (signedMessageList.getCompression().equals(
                JobProtos.SignedMessageList.CompressionType.UNCOMPRESSED)
                ) {
            Log.i(TAG, "Received MessageList not compressed.");
            data = signedMessageList.getMessageList().toByteArray();

        } else if (signedMessageList.getCompression().equals(
                JobProtos.SignedMessageList.CompressionType.ZCOMPRESSION)
                ) {

            Inflater decompressor = new Inflater();
            decompressor.setInput(signedMessageList.getMessageList().toByteArray());
            byte[] buffer = new byte[10000];
            int resultLength = 0;
            try {
                resultLength = decompressor.inflate(buffer);
                Log.i(TAG, "Received MessageList decompressed size: " + resultLength);
            } catch (DataFormatException e) {
                throw new RuntimeException("Unable to decompress MessageList: " + e.getLocalizedMessage());
            }
            decompressor.end();

            // Trim the extra space off the end of the buffer
            data = java.util.Arrays.copyOf(buffer, resultLength);

        } else {
            throw new RuntimeException("Compression scheme not supported.");
        }

        try {
            return JobProtos.MessageList.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException("Unable to parse MessageList from decompressed data.");
        }
    }

    /**
     * Generates a new, unique task_id. (See grr/lib/rdfvalues/flows.py > GrrMessage.GenerateTaskID)
     * @return A 64-bit task ID value
     */
    public static long generateTaskID(Context context) {
        // Random number can not be zero since next_id_base must increment
        // Therefore, add 1 to a random number of range [0, 0xFFFF]
        // Store this and nextIdBase as long so they can be added even if their sum exceeds 32 bits
        long randomNumber = (new Random()).nextInt(0x10000) + 1;

        SharedPreferences clientPrefs = context.getSharedPreferences(
                ClientContract.CLIENT_PREFS, Context.MODE_PRIVATE
        );
        // Get the starting ID base, or 0 if none exists
        long nextIdBase = clientPrefs.getInt(ClientContract.NEXT_ID_BASE, 0);

        // nextIdBase + randomNumber may be over 32 bits, depending on nextIdBase's previous value,
        // but we only want the last 32 bits of it. Upon casting to int, Java truncates to 32 bits.
        int idBase = (int)( nextIdBase + randomNumber );

        // Remember this ID base for next time, so we can ensure no duplicates
        clientPrefs.edit().putInt(ClientContract.NEXT_ID_BASE, idBase).apply();

        // Create a time base using a 32-bit millisecond timestamp. Store it in the leftmost 32 bits
        // of a long value so the id base can be inserted into the rightmost 32 bits. The leftmost 3
        // bits are reserved for the task priority, so ordering by task ID still orders by priority
        long timeBase = (System.currentTimeMillis() & 0x1F_FF_FF_FFL) << 32;

        // Concatenate the 32-bit time base with the 32-bit ID base into a single 64-bit task ID
        long taskId = timeBase | idBase;
        // Prepend the task priority, which occupies the leftmost 3 bits
        taskId |= (long)JobProtos.GrrMessage.Priority.MEDIUM_PRIORITY_VALUE << 61;

        return taskId;
    }

    /** See grr/lib/communicator.py > Communicator.DecodeMessages() */
    public static JobProtos.MessageList decodeMessages(JobProtos.ClientCommunication responseComm,
                                                       PrivateKey clientPrivateKey,
                                                       PublicKey serverPublicKey,
                                                       long nonce) throws GeneralSecurityException {
        //TODO: Look into Cipher caching, so we don't have to redo the following unnecessarily
        // Instantiate a CipherUtils object to decode the message
        CipherUtils cipherUtils = new CipherUtils(responseComm, clientPrivateKey);

        //TODO: THIS IS BROKEN.
//        if (!cipherUtils.verifyCipherSignature(serverPublicKey)) {
//            Log.e(TAG, "Unable to verify the cipher's signature.");
//            throw new SignatureException("Unable to verify the cipher's signature.");
//        }

        // At this point, we know our cipher's legit. Continue decoding the message.
        byte[] plainBytes = cipherUtils.decrypt(
                responseComm.getEncrypted().toByteArray(),
                responseComm.getPacketIv().toByteArray()
        );

        // Parse the decrypted bytes into a SignedMessageList
        JobProtos.SignedMessageList signedMessageList;
        try {
           signedMessageList = JobProtos.SignedMessageList.parseFrom(plainBytes);
        } catch (InvalidProtocolBufferException e) {
            throw new GeneralSecurityException("Invalid signed message list received.");
        }

        // Ensure server returned the same nonce (timestamp) as the one we sent - this helps
        // protect against replay attacks
        if (nonce != signedMessageList.getTimestamp()) {
            throw new GeneralSecurityException("Received incorrect nonce from server - replay attack?");
        }

        // Decompress the SignedMessageList into a regular, readable MessageList and return it
        return decompressMessageList(signedMessageList);
    }
}
