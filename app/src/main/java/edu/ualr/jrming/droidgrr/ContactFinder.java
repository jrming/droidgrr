package edu.ualr.jrming.droidgrr;

/**
 * ContactFinder.java
 * @author Jonathan Ming
 * Modified version of ContactFinder.java, authored by Justin Grover
 * 
 * NOTICE - This software is intended to serve as prototype material.
 * 
 * Modifications: Copyright 2016 Jonathan Ming
 * Original file: Copyright 2013 Justin Grover
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;

/** This class finds the names associated with contact addresses. **/
public class ContactFinder {
	// Initialize constants and variables
	private static final String TAG = "ContactFinder";
	
	/**
	 * This method returns the contact name for a given address.
	 * 
	 * @param context	The application context.
	 * @param address	The provided address (phone number).
	 * @return			The associated contact name.
	 */
	public static String findContactByAddress(Context context, String address) {
		// Initialize the contacts content provider URI
		Uri contactURI = Phone.CONTENT_URI;
		
		// Query contacts data
		String[] projection = { Phone.DISPLAY_NAME };
		String selection = Phone.NUMBER + " = ?";
		String[] selectionArgs = { address };
		String displayName = null;
		Cursor contactsCursor;
		try {
			contactsCursor = context.getContentResolver().query(contactURI, projection, selection, selectionArgs, null);
		} catch(Exception e) {
			Log.e(TAG, "Unable to query contacts content provider");
			return null;
		}
		if (contactsCursor == null) {
			Log.e(TAG, "Contacts Content Provider returned null cursor");
			return null;
		}
		
		if (contactsCursor.moveToFirst() && contactsCursor.getCount() > 0)  {
            try {
                displayName = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Phone.DISPLAY_NAME));
            } catch(Exception e) {
            	Log.e(TAG, "Unable to find the contact name");
            	return null;
            }
		}
		// Done with the cursor, close it
		contactsCursor.close();
		return displayName;
	}

	/**
	 * This method returns the contact name for a given contact id.
	 * @param context	The application context.
	 * @param id		The provided contact ID.
	 * @return			The associated contact name.
	 */
	public static String findContactById(Context context, int id) {
		// Initialize the contacts content provider URI
		Uri contactURI = Phone.CONTENT_URI;

		// Query contacts data
		String[] projection = { Phone.DISPLAY_NAME };
		String selection = Phone.RAW_CONTACT_ID + " = ?";
		String[] selectionArgs = { Integer.toString(id) };
		String displayName = null;
		Cursor contactsCursor;
		try {
			contactsCursor = context.getContentResolver().query(contactURI, projection, selection, selectionArgs, null);
		} catch(Exception e) {
			Log.e(TAG, "Unable to query contacts content provider");
			return null;
		}
		if (contactsCursor == null) {
			Log.e(TAG, "Contacts Content Provider returned null cursor");
			return null;
		}

		if (contactsCursor.moveToFirst() && contactsCursor.getCount() > 0)  {
			try {
				displayName = contactsCursor.getString(contactsCursor.getColumnIndexOrThrow(Phone.DISPLAY_NAME));
			} catch(Exception e) {
				Log.e(TAG, "Unable to find the contact name");
				return null;
			}
		}
		// Done with the cursor, close it
		contactsCursor.close();
		return displayName;
	}

}