# DroidGRR #

Android application code for Jonathan Ming's project at the NSF-sponsored CyberSAFE REU program held at the University of Arkansas, Little Rock.

Research Findings (PDF): https://www.mengjunxie.org/papers/CNS_2016_Poster_Ming.pdf

## What it does ##

The end goal for this project is to develop an Android app which functions as a client for the [GRR Rapid Response](https://github.com/google/grr) incident response framework to allow remote live forensics to be performed on Android devices in a way previously only possible on traditional desktop/laptop computers.

## Current status ##

DroidGRR is a currently a prototype, which means that the app is not suitable for real-world deployment in its current state. There remains more work to be done on the functions and features of the app before it can be used on an actual device. Certain best-practice and security guidelines must be implemented before DroidGRR can leave the prototype stage. Most of this work is marked in the code with a TODO comment.

What the app *is* able to do, however, is serve as a foundation for future work in this area. DroidGRR serves as a case in point that mobile device integration into existing remote live forensic frameworks is both possible and beneficial. It is evidence that usage data can be collected remotely from a Android devices and analyzed within a scalable forensic framework. It also provides a practical code foundation for an Android GRR client: despite its lack of flow implementation, it contains the logic necessary to establish a working connection between a GRR server and an Android device. This connection logic is written specifically with GRR in mind; much of it was ported directly from the GRR client's Python code. DroidGRR also provides data collection code for obtaining commonly desired information about Android devices.

## Relation to DroidWatch ##

This project is, code-wise, based off of Justin Grover's 2013 prototype titled [DroidWatch](https://github.com/jgrover/DroidWatch), however this project is neither affiliated with nor otherwise connected to Grover's project.